export class Language {
    id: string;
    title: string;
    flag: string;
    status: boolean;
    constructor(_id, _title, _flag, _status){
        this.id = _id;
        this.title = _title;
        this.flag = _flag;
        this.status = _status;
    }

}

export const system_lang = [
    new Language('en', 'English', 'us', false),
    new Language('pt', 'Portuguese', 'br', false),
    new Language('sp', 'Spanish', 'es', false),
];
