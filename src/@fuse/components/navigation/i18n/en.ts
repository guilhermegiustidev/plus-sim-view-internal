export const locale = {
    lang: 'en',
    data: {
        'LABELS': {
            'COUNTRY': {
                'USA': 'United States of America'
            }, 
            'ERROR' : {
                'PERMISSION': {
                    'TITLE': 'Acesso Negado',
                    'MESSAGE': 'Seu usuário não tem permissão para executar esta tarefa'
                }
            },
        },
        
        'MODULE' : {
            'ADM' : {
                'TITLE': 'Administrador',
                'PROGRAM' : {
                    'PLAN' : 'Plans',
                    '1': 'Operações',
                    '2': 'Acesso e Segurança',
                    '4': 'E-commerce',
                    '21': 'Estoque',
                    'ADM01': 'Filiais',
                    'ADM02': 'Usuários',
                    'ADM03': 'Linhas',
                    'ADM04': 'Representantes',
                    'ADM05': 'Plus Sim Plans',
                    'ADM06': 'Perfis',
                    'ADM07': 'Pabx',
                    'ADM08': 'Imagens do E-commerce',
                    'ADM09': 'Planos Fornecedor',
                    'ADM10': 'Fornecedores',
                    'ADM11': 'Tipos de Serviço',
                    'ADM12': 'Cadastro de Chips',
                    'ADM13': 'Consulta de Lots',
                    'ADM14': 'Expedição de Chips',
                }
            },
            'PAT' : {
                'TITLE': 'Representantes',
                'PROGRAM' : {
                    '12': 'Orders',
                    'ORD01': 'Pedidos',
                    'ORD02': 'Chips'
                }
            }
        },
        'PERMISSION' : {
            'WRITE.TITLE': 'Escrita',
            'READ.TITLE': 'Leitura',
            'ROLE_PERSON_READ': 'Ler Usuário',
            'ROLE_PERSON_WRITE': 'Gravar Usuário',
            'ROLE_PROFILE_READ': 'Ler Perfil',
            'ROLE_PROFILE_WRITE': 'Gravar Perfil',
            'ROLE_BRANCH_READ': 'Ler Filial',
            'ROLE_BRANCH_WRITE': 'Gravar Filial',
            'ROLE_ADDRESS_READ': 'Ler Endereço',
            'ROLE_ADDRESS_WRITE': 'Gravar Endereço',
            'ROLE_CONTACT_READ' : 'Ler Contatos',
            'ROLE_CONTACT_WRITE' : 'Gravar Contatos',
            'ROLE_PARTNER_READ' : 'Ler Parceiros',
            'ROLE_PARTNER_WRITE' : 'Gravar Parceiros',
            'ROLE_PLAN_READ' : 'Ler Planos',
            'ROLE_PLAN_WRITE' : 'Gravar Planos',
            'ROLE_LINE_READ' : 'Ler Linhas',
            'ROLE_LINE_WRITE' : 'Gravar Linhas',
            'ROLE_PABX_READ' : 'Ler Pabx',
            'ROLE_PABX_WRITE' : 'Gravar Pabx',
            'ROLE_PROVIDER_READ' : 'Ler Fornecedor',
            'ROLE_PROVIDER_WRITE' : 'Gravar Fornecedor',
            'ROLE_PLANPROVIDER_READ': 'Ler Planos de Fornecedores',
            'ROLE_PLANPROVIDER_WRITE': 'Gravar Planos de Fornecedores',
            'ROLE_SIN_READ': 'Ler Chip',
            'ROLE_SIN_WRITE': 'Gravar Chip',
            'ROLE_LOT_READ': 'Ler Lote',
            'ROLE_LOT_WRITE': 'Gravar Lote',
            'ROLE_PLANSERVICETYPE_READ': 'Ler Tipos de serviços de planos',
            'ROLE_PLANSERVICETYPE_WRITE': 'Gravar Tipos de serviços de planos',
            'HAS_NO_PERMISSION': {
                'BODY':  'Você não possuí permissão para accessar essa página',
                'TITLE': 'Acesso negado'
            }
        },
        'ACTION': {
            'OPTIONS': 'Opções',
            'SAVE': 'Salvar',
            'CANCEL': 'Cancelar',
            'BACK': 'Voltar',
            'NEW': 'Novo',
            'EDIT': 'Editar',
            'REMOVE': 'Remover',
            'TITLE': {
                'EXCLUDE': 'Exclusão'
            },
            'BODY': {
                'EXCLUDE': 'Todos os registros foram apagados ou inativados'
            }

        },
        'CONFIRM': {
            'EXCLUDE': {
                'TITLE' : 'Confirmar Exclusão',
                'BODY': 'Deseja realizar exclusão dos registros',
                'YES': 'Sim',
                'NO': 'Não',
                'ERROR': {
                    'TITLE': 'Erro ao Excluir',
                    'BODY': 'Não é possível excluir registros, possivelmente existem vinculos que não podem ser desfeitos',
                    
                }
            }
        }
    }
};
