export const locale = {
    lang: 'en',
    data: {
        'LABELS': {
            'COUNTRY': {
                'USA': 'United States of America'
            }, 
            'ERROR' : {
                'PERMISSION': {
                    'TITLE': 'Acesso Negado',
                    'MESSAGE': 'Seu usuário não tem permissão para executar esta tarefa'
                }
            },
        },
        
        'MODULE' : {
            'ADM' : {
                'TITLE': 'Administrator',
                'PROGRAM' : {
                    'PLAN' : 'Plans',
                    '1': 'Operações',
                    '2': 'Acesso e Segurança',
                    '4': 'E-commerce',
                    'ADM01': 'Filiais',
                    'ADM02': 'Usuários',
                    'ADM03': 'Linhas',
                    'ADM04': 'Representantes',
                    'ADM05': 'Plus Sim Plans',
                    'ADM06': 'Perfis',
                    'ADM07': 'Pabx',
                    'ADM08': 'Imagens do E-commerce',
                    'ADM09': 'Provider Plans'
                }
            },
            'PAT' : {
                'TITLE': 'Representantes',
                'PROGRAM' : {
                    '12': 'Orders',
                    'ORD01': 'Pedidos'
                }
            }
        },
        'PERMISSION' : {
            'ROLE_PERSON_READ': 'Ler Usuário',
            'ROLE_PERSON_WRITE': 'Gravar Usuário',
            'ROLE_PROFILE_READ': 'Ler Perfil',
            'ROLE_PROFILE_WRITE': 'Gravar Perfil',
            'ROLE_BRANCH_READ': 'Ler Filial',
            'ROLE_BRANCH_WRITE': 'Gravar Filial',
            'ROLE_ADDRESS_READ': 'Ler Endereço',
            'ROLE_ADDRESS_WRITE': 'Gravar Endereço',
            'HAS_NO_PERMISSION': {
                'BODY':  'Você não possuí permissão para accessar essa página',
                'TITLE': 'Acesso negado'
            }
        },
        'ACTION': {
            'OPTIONS': 'Opções',
            'SAVE': 'Salvar',
            'CANCEL': 'Cancelar',
            'BACK': 'Voltar',
            'NEW': 'Novo',
            'EDIT': 'Editar',
            'REMOVE': 'Remover',
            'TITLE': {
                'EXCLUDE': 'Exclusão'
            },
            'BODY': {
                'EXCLUDE': 'Todos os registros foram apagados ou inativados'
            }

        },
        'CONFIRM': {
            'EXCLUDE': {
                'TITLE' : 'Confirmar Exclusão',
                'BODY': 'Deseja realizar exclusão dos registros',
                'YES': 'Sim',
                'NO': 'Não',
                'ERROR': {
                    'TITLE': 'Erro ao Excluir',
                    'BODY': 'Não é possível excluir registros, possivelmente existem vinculos que não podem ser desfeitos',
                    
                }
            }
        }
    }
};
