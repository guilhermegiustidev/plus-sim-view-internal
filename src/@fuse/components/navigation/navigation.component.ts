import { Component, Input, ViewEncapsulation } from '@angular/core';
import { AuthenticationBusiness } from 'app/service/authentication/auth.service';
import { TranslateService } from '@ngx-translate/core';



@Component({
    selector: 'fuse-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseNavigationComponent {
    @Input() layout = 'vertical';
    @Input() navigation: any;

    constructor(
        private authenticationBusiness: AuthenticationBusiness, 

    ) {

        this.authenticationBusiness.findMenus().subscribe(
            result => {
                this.navigation = result.json();
            }
        );
    }

}