import { Component, ViewChild, Input } from '@angular/core'

import { CityModel } from 'app/model/city.model';
import { Page } from '@fuse/components/page/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { CountryBusiness } from 'app/service/location/country.service';
import { CityBusiness } from 'app/service/location/city.service';
import { StateBusiness } from 'app/service/location/state.service';
import { StateModel } from 'app/model/state.model';
import { CountryModel } from 'app/model/country.model';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

@Component({
    selector: 'city-component',
    templateUrl: './city.component.html',

})
export class CityComponent implements OnInit{

    @ViewChild(DatatableComponent) table: DatatableComponent;
    countryId: number;
    stateId : number;
    
    @Input() city: CityModel;
    page: Page<CityModel>;
    cityName: string;
    pageLimit: number = 5;
    
    countries   : CountryModel[];
    states      : StateModel [];
    cities      : CityModel[];

    
    constructor(
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private countryBusiness: CountryBusiness,
        private cityBusiness: CityBusiness,
        private stateBusiness: StateBusiness
    ){
        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

    }

    ngOnInit(): void {
        this.countryBusiness.findAll().subscribe(
            success => {
                this.countries = success;
            }
        );
    }

    showCountrySelected(){
        if(this.countryId){
            if(this.city){
                return false;
            }
        }
        return true;
    }

    changeCountry(event){
        this.loadCity(0);
        this.stateBusiness.findStatesByCountryId(this.countryId).subscribe(
            success => {
                this.states = success;
            }
        );
    }


    loadCity(pageNumber: number){
        if(this.table){
            this.table.loadingIndicator = true;
        }
        this.cityBusiness.findCitiesByCountryIdAndLikeNameAndStateCount(this.countryId, this.cityName, this.stateId).subscribe(
            success => {
                this.cityBusiness.findCitiesByCountryIdAndLikeNameAndStatePage(this.countryId, this.cityName, this.stateId, this.pageLimit, pageNumber, success).subscribe(
                    success => {
                        this.page = success;
                        if(this.table){
                            this.table.loadingIndicator = false;
                        }
                    },
                )

            }
        )
    }

    clearCity(event){
        this.city = null;
        this.countryId = null;
        this.loadCity(0);
    }

    onActivate(event){
        if(event.type === 'click'){
            this.city = event.row;
            this.page = null;
        }
    }

    setPage(dataTable: any){
        this.loadCity(dataTable.offset);
    }

}