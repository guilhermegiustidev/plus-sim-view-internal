import { Component, OnInit, Input, Inject, ViewEncapsulation } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { ContactBusiness } from 'app/service/contact/contact.service';
import { Language } from '../../../language/language.component';
import { ContactModel } from 'app/model/contact.model';
import { fuseAnimations } from '@fuse/animations';

@Component({
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ContactDialogComponent implements OnInit {


    id: number;
    numberPhone: string;
    email: string;
    name: string;
    type: string;
    langs: Array<Language>;
    hash: string;

    contactForm: FormGroup;
    contactFormErrors: any;
    processing: boolean;

    action: string;
    contactTypes: string[];


    constructor(private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<ContactDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private contactBusiness: ContactBusiness) {


        this.id = data.id;
        this.numberPhone = data.numberPhone;
        this.email = data.email;
        this.name = data.name;
        this.type = data.type;
        this.hash = data.hash;
        this.langs = data.langs;
        this.action = data.action;

        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);
        this.contactForm = this.createForm();
        this.contactForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });

        this.contactBusiness.findContactTypes().subscribe(
            result => {
                this.contactTypes = result;
            }
        );

    }


    createForm() {
        return this.formBuilder.group({
            name: [this.name, Validators.required],
            type: [this.type, Validators.required],
            email: [this.email, Validators.email],
            numberPhone: [this.numberPhone, Validators.pattern("^[0-9]*$")],
        });
    }

    onChangeForm() {
        for (const field in this.contactFormErrors) {
            if (!this.contactFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.contactFormErrors[field] = {};
            const control = this.contactForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.contactFormErrors[field] = control.errors;
            }
        }
    }

    ngOnInit() {
        this.contactFormErrors = {
            type: {},
            name: {},
            numberPhone: {}
        };

    }
    cancel(){
        this.dialogRef.close();
    }
    confirmContact() {
        let model: ContactModel = new ContactModel();
        model.id = this.id;
        model.name = this.name;
        model.type = this.type;
        model.email = this.email;
        model.phone = this.numberPhone;
        model.hash = this.hash;
        model.langs = this.langs;


        if (this.hash) {
            this.dialogRef.close([this.action, model])
        } else {
            this.contactBusiness.getHash().subscribe(
                success => {
                    model.hash = success['_body'];
                    this.dialogRef.close([this.action, model])
                }
            );
        }
    }

    disabledConfirm() {
        let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        let NUMBER_REGEXP = /^[0-9]*$/;
        if (this.type) {
            switch (this.type) {
                case "EMAIL":
                    return !(this.name && this.type && this.email && EMAIL_REGEXP.test(this.email));
                case "PHONE":
                    return !(this.name && this.type && this.numberPhone && NUMBER_REGEXP.test(this.numberPhone));
            }
        }
        return false;
    }

}
