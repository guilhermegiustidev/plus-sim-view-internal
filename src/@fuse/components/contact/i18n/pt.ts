export const locale = {
    lang: 'pt',
    data: {
        'CONTACT':{
            'OPTION': {
                'PRIMARY': 'Tornar Principal',
                'REMOVE': 'Remover',
                'PRIMARY_CONTACT': 'Contato Principal',
                'EDIT': 'Editar Contato'
            },
            'TYPE': {
                'EMAIL': 'Email',
                'PHONE': 'Telefone'
            },
            'TITLE': 'Contato',
            'FORM': {
                'ADD_CONTACT': 'Adicionar Contato',
                'NAME': 'Nome',
                'LOCALES': 'Idiomas',
                'TYPE': 'Tipo de Contato',
                'PHONE': 'Número de telefone',
                'EMAIL': 'Email',
                'VALIDATOR': {
                    'NAME': {
                        'REQUIRED': 'Nome é obrigatório'
                    },
                }
            }
        }
    }
};
