export const locale = {
    lang: 'en',
    data: {
        'CONTACT':{
            'OPTION': {
                'PRIMARY': 'Tornar Principal',
                'REMOVE': 'Remover',
                'PRIMARY_CONTACT': 'Contato Principal',
                'EDIT': 'Editar Contato'
            },
            'TYPE': {
                'EMAIL': 'Email',
                'PHONE': 'Telefone'
            },
            'TITLE': 'Contato',
            'FORM': {
                'ADD_CONTACT': 'Adicionar Contato',
                'NAME': 'Descrição',
                'LOCALES': 'Idiomas',
                'TYPE': {
                    'EMAIL': 'Email',
                    'PHONE': 'Telefone',
                    'TITLE': 'Tipo'
                },
                'PHONE': 'Número de telefone',
                'EMAIL': 'Email',
                'VALIDATOR': {
                    'NAME': {
                        'REQUIRED': 'Nome é obrigatório'
                    },
                }
            }
        }
    }
};
