import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactDialogComponent } from './dialog/contact.component';

import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@fuse/material.module';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { ServiceModule } from 'app/service/service.module';
import { ContactComponent } from '@fuse/components/contact/contact.component';

@NgModule({
    declarations: [
        ContactDialogComponent,
        ContactComponent
    ],
    exports: [
        ContactDialogComponent,
        ContactComponent
    ],
    imports: [
        TranslateModule,
        MaterialModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ServiceModule
    ],
    entryComponents:[
        ContactDialogComponent
    ]
})
export class ContactModule {
}
