import { Component, ViewEncapsulation } from '@angular/core';
import { ContactModel } from 'app/model/contact.model';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { ContactDialogComponent } from '@fuse/components/contact/dialog/contact.component';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { system_lang, Language } from '@fuse/language/language.component';
import { combineAll } from 'rxjs/operators/combineAll';

@Component({
    selector: 'plussim-contact',
    templateUrl: './contact.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ContactComponent {

    contacts: Array<ContactModel>;
    dialogRef: any;

    constructor(
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private dialog: MatDialog,
    ) {
        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

    }

    addContact() {
        let langs = new Array<Language>();
        langs.push(new Language('en', 'English', 'us', false));
        langs.push(new Language('pt', 'Portuguese', 'br', false));
        langs.push(new Language('sp', 'Spanish', 'es', false));
        
        this.dialogRef = this.dialog.open(ContactDialogComponent, {
            panelClass: 'form-dialog',
            data: {
                action: 'putContact',
                dialogRef: this.dialogRef,
                langs: langs
            }
        });

        this.closeDialogContact();

    }

    private closeDialogContact() {
        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (response) {
                    let action: string = response[0];
                    let contact = response[1];
                    if (!this.contacts) {
                        this.contacts = new Array<ContactModel>();
                    }
                    if (response[0] === 'saveContact') {
                        for (let item of this.contacts) {
                            let compare = false;
                            if(contact.id){
                                compare = item.id === contact.id;
                            } else {
                                compare = item.hash === contact.hash;
                            }
                            if (compare) {
                                item.id = contact.id;
                                item.name = contact.name;
                                item.type = contact.type;
                                item.email = contact.email;
                                item.phone = contact.phone;
                                item.hash = contact.hash;
                                item.langs = contact.langs;
                                item.status = true;
                            }
                        }
                    }
                    else if (response[0] === 'putContact') {
                        contact.status = true;
                        this.contacts.push(contact);
                    }
                }
            });
    }


    editContact(event, contact, index) {
        event.preventDefault();
        this.dialogRef = this.dialog.open(ContactDialogComponent, {
            panelClass: 'form-dialog',
            data: {
                action: 'saveContact',
                id: contact.id,
                numberPhone: contact.phone,
                email: contact.email,
                name: contact.name,
                type: contact.type,
                langs: contact.langs,
                hash: contact.hash,
                status: contact.status

            }
        });
        this.closeDialogContact();
    }

    setPrimary(event, contact) {
        event.preventDefault();
        for (let index of this.contacts) {
            index.primary = false;
        }
        contact.primary = true;
    }


}