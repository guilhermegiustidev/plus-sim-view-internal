import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutocompleteUserComponent } from './users/autocomplete.user.component';

import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@fuse/material.module';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FusePipesModule } from '@fuse/pipes/pipes.module';
import { AutocompleteProfileComponent } from '@fuse/components/autocomplete/profile/autocomplete.profile.component';
import { AutocompleteTypeServiceComponent } from '@fuse/components/autocomplete/servicetype/autocomplete.service.type.component';

@NgModule({
    declarations: [
        AutocompleteUserComponent,
        AutocompleteProfileComponent,
        AutocompleteTypeServiceComponent
    ],
    exports: [
        AutocompleteUserComponent,
        AutocompleteProfileComponent,
        AutocompleteTypeServiceComponent
    ],
    imports: [
        TranslateModule,
        MaterialModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FusePipesModule
    ]
})
export class AutocompleteModule {
}
