export const locale = {
    lang: 'en',
    data: {
        'AUTOCOMPLETE' :{
            'USER' : {
                'FORM' : {
                    'SEARCH_USERS' : {
                        'TITLE': 'Pesquise um usuário'
                    },
                    'SELECTED_USERS': {
                        'TITLE': 'Usuarios Selecionados'
                    }
                }
            }
        }
    }
};
