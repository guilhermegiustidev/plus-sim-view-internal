import { Component } from '@angular/core';
import { UserBusiness } from 'app/service/user/user.service';
import { UserModel } from 'app/model/user.model';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

@Component({
    selector: 'autocomplete-users',
    templateUrl: './autocomplete.user.component.html',
})
export class AutocompleteUserComponent {

    selected: Array<UserModel>;
    searchedUsers   : Array<UserModel>;

    constructor(private userBusiness: UserBusiness, private fuseTranslationLoaderService: FuseTranslationLoaderService){
        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);
    }

    searchUser(event) {
        let value = event.target.value;
        this.searchedUsers = new Array<UserModel>();

        if (value && value.length > 0) {
            this.userBusiness.findByLikeName(value).subscribe(
                success => {
                    if(!this.searchedUsers){
                        this.searchedUsers = new Array<UserModel>();
                    }
                    if(success){
                        for(let index of success){
                            if(!this.containsUser(index)){
                                this.searchedUsers.push(index);
                            }
                        }
                    }
                }
            );
        }
    }

    selectValue(option: UserModel, input){
        input.value = '';
        this.searchedUsers = []; 
        if(this.selected == null){
            this.selected = new Array<UserModel>();
        }
        if(!this.containsUser(option)){
            this.selected.push(option);
        }
    }

    containsUser(userModel: UserModel) : boolean{
        if(!this.selected){
            this.selected = new Array<UserModel>();
        }
        for(let user of this.selected){
            if(user.id === userModel.id){
                if(!user.status){
                    user.status = true;
                }
                return true;
            }
        }
        return false;
    }
}