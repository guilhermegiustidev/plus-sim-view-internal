import { Component } from '@angular/core';
import { ProfileModel } from 'app/model/profile/profile.model';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { ProfileBusiness } from 'app/service/profile/profile.service';

@Component({
    selector: 'autocomplete-profiles',
    templateUrl: './autocomplete.profile.component.html',
})
export class AutocompleteProfileComponent {

    selected: Array<ProfileModel>;
    searched   : Array<ProfileModel>;

    constructor(private profileBusiness: ProfileBusiness, private fuseTranslationLoaderService: FuseTranslationLoaderService){
        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);
    }

    search(event) {
        let value = event.target.value;
        this.searched = new Array<ProfileModel>();

        if (value && value.length > 0) {
            this.profileBusiness.findByLikeName(value).subscribe(
                success => {
                    if(!this.searched){
                        this.searched = new Array<ProfileModel>();
                    }
                    if(success){
                        for(let index of success){
                            if(!this.containsUser(index)){
                                this.searched.push(index);
                            }
                        }
                    }
                }
            );
        }
    }

    selectValue(option: ProfileModel, input){
        input.value = '';
        this.searched = []; 
        if(this.selected == null){
            this.selected = new Array<ProfileModel>();
        }
        if(!this.containsUser(option)){
            this.selected.push(option);
        }
    }

    containsUser(profileModel: ProfileModel) : boolean{
        if(!this.selected){
            this.selected = new Array<ProfileModel>();
        }
        for(let index of this.selected){
            if(index.id === profileModel.id){
                if(!index.status){
                    index.status = true;
                }
                return true;
            }
        }
        return false;
    }
}