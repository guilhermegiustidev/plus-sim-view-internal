export const locale = {
    lang: 'en',
    data: {
        'AUTOCOMPLETE' :{
            'PROFILE' : {
                'FORM' : {
                    'SEARCH_PROFILES' : {
                        'TITLE': 'Pesquise um perfil'
                    },
                    'SELECTED_PROFILES': {
                        'TITLE': 'Perfis Selecionados'
                    }
                }
            }
        }
    }
};
