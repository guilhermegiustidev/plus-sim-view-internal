import { Component, Input } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { TypeServicePlanModel } from 'app/model/plan/type.service.plan.model';
import { TypeServicePlanBusiness } from 'app/service/plan/type.service.plan.service';

@Component({
    selector: 'autocomplete-service-type',
    templateUrl: './autocomplete.service.type.component.html',
})
export class AutocompleteTypeServiceComponent {

    @Input() selected: Array<TypeServicePlanModel>;
    searched   : Array<TypeServicePlanModel>;

    constructor(private business: TypeServicePlanBusiness, private fuseTranslationLoaderService: FuseTranslationLoaderService){
        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);
    }

    searchUser(event) {
        let value = event.target.value;
        this.searched = new Array<TypeServicePlanModel>();

        if (value && value.length > 0) {
            this.business.findByLikeName(value).subscribe(
                success => {
                    if(!this.searched){
                        this.searched = new Array<TypeServicePlanModel>();
                    }
                    if(success){
                        for(let index of success){
                            if(!this.containsUser(index)){
                                this.searched.push(index);
                            }
                        }
                    }
                }
            );
        }
    }

    selectValue(option: TypeServicePlanModel, input){
        input.value = '';
        this.searched = []; 
        if(this.selected == null){
            this.selected = new Array<TypeServicePlanModel>();
        }
        if(!this.containsUser(option)){
            this.selected.push(option);
        }
    }

    containsUser(model: TypeServicePlanModel) : boolean{
        if(!this.selected){
            this.selected = new Array<TypeServicePlanModel>();
        }
        for(let user of this.selected){
            if(user.id === model.id){
                if(!user.status){
                    user.status = true;
                }
                return true;
            }
        }
        return false;
    }
}