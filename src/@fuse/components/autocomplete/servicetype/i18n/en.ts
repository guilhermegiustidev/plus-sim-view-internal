export const locale = {
    lang: 'en',
    data: {
        'AUTOCOMPLETE' :{
            'SERVICE_TYPE' : {
                'FORM' : {
                    'SEARCH_SERVICES' : {
                        'TITLE': 'Pesquise um serviço'
                    },
                    'SELECTED_SERVICES': {
                        'TITLE': 'Serviços Selecionados'
                    }
                }
            }
        }
    }
};
