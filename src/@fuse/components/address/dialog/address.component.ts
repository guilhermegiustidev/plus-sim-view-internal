import { Component, OnInit, Input, Inject } from '@angular/core';
import { AddressModel } from 'app/model/address.model';

import { CountryModel } from 'app/model/country.model';
import { StateModel } from 'app/model/state.model';
import { CityModel } from 'app/model/city.model';
import { CityBusiness } from 'app/service/location/city.service';
import { StateBusiness } from 'app/service/location/state.service';
import { CountryBusiness } from 'app/service/location/country.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';

import { AddressBusiness } from 'app/service/address/address.service';

@Component({
    templateUrl: './address.component.html',
    styleUrls: ['./address.component.scss']
})
export class AddressDialogComponent implements OnInit {

    id: number;
    selectedCountryId: number;
    selectedStateId: number;
    selectedCityId: number;
    address: string;
    zip: string;
    principal: boolean;
    complement: string;
    country: CountryModel;
    state: StateModel;
    city: CityModel;
    primary: boolean;
    countries: CountryModel[];
    states: StateModel[];
    cities: CityModel[];
    hash : string;


    addressForm: FormGroup;
    addressFormErrors: any;
    processing: boolean;

    action: string;


    constructor(private fuseTranslationLoaderService: FuseTranslationLoaderService, private countryBusiness: CountryBusiness,
        private cityBusiness: CityBusiness, private stateBusiness: StateBusiness,
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<AddressDialogComponent>,
        private addressBusiness: AddressBusiness,
        @Inject(MAT_DIALOG_DATA) private data: any) {

        this.id = data.id;
        this.action = data.action;
        this.selectedCountryId = data.selectedCountryId;
        this.selectedStateId = data.selectedStateId;
        this.selectedCityId = data.selectedCityId;
        this.address = data.address;
        this.zip = data.zip;
        this.primary = data.primary;
        this.complement = data.complement;
        this.hash = data.hash;

        if (this.selectedCountryId) {
            this.loadStates(this.selectedCountryId);
        }
        if (this.selectedStateId) {
            this.loadCities(this.selectedStateId);
        }
        this.loadCountry();
        this.loadState();
        this.loadCity();

        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);
        this.addressForm = this.createForm();
        this.addressForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });

    }

    createForm() {
        return this.formBuilder.group({
            country: [this.selectedCountryId, Validators.required],
            state: [this.selectedStateId, Validators.required],
            city: [this.selectedCityId, Validators.required],
            address: [this.address, Validators.required],
            zip: [this.zip, [Validators.pattern("^[0-9]*$"), Validators.required]],
            complement: [this.complement]
        });
    }

    onChangeForm() {
        for (const field in this.addressFormErrors) {
            if (!this.addressFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.addressFormErrors[field] = {};
            const control = this.addressForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.addressFormErrors[field] = control.errors;
            }
        }
    }

    ngOnInit() {
        this.countryBusiness.findAll().subscribe(
            success => {
                this.countries = success;
            }
        );
        if (this.selectedCountryId) {
            this.loadStates(this.selectedCountryId);
        }
        if (this.selectedStateId) {
            this.loadCities(this.selectedStateId);
        }
        this.addressFormErrors = {
            country: {},
            state: {},
            city: {},
            address: {},
            zip: {},
            complement: {}
        };

    }

    changeCountry(event) {
        this.loadStates(this.selectedCountryId);
        this.loadCountry();
        this.selectedStateId = null;
        this.selectedCityId = null;
    }

    private loadCountry() {
        if (this.selectedCountryId) {
            this.countryBusiness.findById(this.selectedCountryId).subscribe(success => {
                this.country = success;
            });
        }
    }

    changeState(event) {
        this.loadCities(this.selectedStateId);
        this.loadState();
        this.selectedCityId = null;
    }

    private loadState() {
        if (this.selectedStateId) {
            this.stateBusiness.findById(this.selectedStateId).subscribe(success => {
                this.state = success;
            });
        }
    }

    changeCity(event) {
        this.loadCity();
    }



    private loadCity() {
        if (this.selectedCityId) {
            this.cityBusiness.findById(this.selectedCityId).subscribe(success => {
                this.city = success;
            });
        }
    }

    loadStates(countryId: number) {
        this.stateBusiness.findStatesByCountryId(countryId).subscribe(
            success => {
                this.states = success;
            },
            error => {
                alert('Erro ao buscar estados');
            }
        );
    }

    loadCities(stateId: number) {
        this.cityBusiness.findCitiesByIdState(stateId).subscribe(
            success => {
                this.cities = success;
            },
            error => {
                alert('Erro ao buscar cities');
            }
        );
    }

    cancel(){
        this.dialogRef.close();
    }

    confirmAddress() {
        let model: AddressModel;
        model = {
            id          : this.id,
            country     : this.country,
            state       : this.state,
            city        : this.city,
            address     : this.address,
            zip         : this.zip,
            status      : true,
            primary     : this.primary,
            complement  : this.complement,
            hash        : this.hash
        };

        if(this.hash){
            this.dialogRef.close([this.action, model])
        } else {
            this.addressBusiness.getHash().subscribe(
                success => {
                    model.hash = success['_body'];
                    this.dialogRef.close([this.action, model])
                }
            );
        }


    }

}
