import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddressDialogComponent } from './dialog/address.component';

import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@fuse/material.module';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AddressComponent } from '@fuse/components/address/address.component';

@NgModule({
    declarations: [
        AddressDialogComponent,
        AddressComponent
    ],
    exports: [
        AddressDialogComponent,
        AddressComponent
    ],
    imports: [
        TranslateModule,
        MaterialModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    entryComponents:[
        AddressDialogComponent
    ]
})
export class AddressModule {
}
