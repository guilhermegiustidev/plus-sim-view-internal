export const locale = {
    lang: 'sp',
    data: {
        'ADDRESS':{
            'FORM': {
                'COUNTRY': 'United States',
                'STATE': 'Estado',
                'CITY': 'Cidade',
                'VALIDATOR': {
                    'COUNTRY': {
                        'REQUIRED': 'País é obrigatório'
                    },
                    'STATE': {
                        'REQUIRED': 'Estado é obrigatório'
                    },
                    'CITY': {
                        'REQUIRED': 'Cidade é obrigatório'
                    }
                }
            }
        }
    }
};
