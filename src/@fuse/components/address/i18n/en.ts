export const locale = {
    lang: 'en',
    data: {
        'ADDRESS':{
            'OPTION': {
                'PRIMARY': 'Tornar Principal',
                'REMOVE': 'Remover',
                'PRIMARY_ADDRESS': 'Endereço Principal',
                'EDIT': 'Editar Endereço'
            },
            'TITLE': 'Endereço',
            'FORM': {
                'ADD_ADDRESS': 'Adicionar Endereço',
                'COUNTRY': 'País',
                'STATE': 'Estado',
                'CITY': 'Cidade',
                'ADDRESS': 'Endereço',
                'ZIP': 'Cep',
                'COMPLEMENT': 'Complemento',
                'VALIDATOR': {
                    'COUNTRY': {
                        'REQUIRED': 'País é obrigatório'
                    },
                    'STATE': {
                        'REQUIRED': 'Estado é obrigatório'
                    },
                    'CITY': {
                        'REQUIRED': 'Cidade é obrigatório'
                    }
                }
            }
        }
    }
};
