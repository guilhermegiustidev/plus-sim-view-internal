export const locale = {
    lang: 'pt',
    data: {
        'ADDRESS':{
            'FORM': {
                'COUNTRY': 'United States',
                'STATE': 'Estado',
                'CITY': 'Cidade',
                'VALIDATOR': {
                    'COUNTRY': {
                        'REQUIRED': 'País é obrigatório'
                    },
                    'STATE': {
                        'REQUIRED': 'Estado é obrigatório'
                    },
                    'CITY': {
                        'REQUIRED': 'Cidade é obrigatório'
                    }
                }
            }
        }
    }
};
