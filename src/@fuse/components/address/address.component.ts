import { Component, ViewEncapsulation } from '@angular/core';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AddressDialogComponent } from '@fuse/components/address/dialog/address.component';
import { AddressModel } from 'app/model/address.model';
import { fuseAnimations } from '@fuse/animations';

@Component({
    selector : 'plussim-address',
    templateUrl: './address.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class AddressComponent {
    
    dialogRef: any;

    addressess: Array<AddressModel>;

    constructor(
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private dialog: MatDialog,
    ) {

        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

    }


    addEdereco() {
        this.dialogRef = this.dialog.open(AddressDialogComponent, {
            panelClass: 'form-dialog',
            data: {
                action: 'putAddress',
                dialogRef: this.dialogRef
            }
        });
        this.onCloseDialogAddress();
    }

    setPrimary(event, address) {
        event.preventDefault();
        for (let index of this.addressess) {
            index.primary = false;
        }
        address.primary = true;

    }


    private onCloseDialogAddress() {
        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (response) {
                    let action: string = response[0];
                    let address = response[1];
                    if (!this.addressess) {
                        this.addressess = new Array<AddressModel>();
                    }
                    if (response[0] === 'saveAddress') {
                        for (let item of this.addressess) {

                            let compare = false;
                            if(address.id){
                                compare = item.id === address.id;
                            } else {
                                compare = item.hash === address.hash;
                            }
                            if (compare) {
                                item.id = address.id;
                                item.address = address.address;
                                item.country = address.country;
                                item.state = address.state;
                                item.city = address.city;
                                item.zip = address.zip;
                                item.primary = address.primary;
                                item.status = address.status;
                                item.hash = address.hash;
                                item.complement = address.complement;
                            }
                        }
                    } else if (response[0] === 'putAddress') {
                        this.addressess.push(address);
                    }
                }
            });
    }

    editAddress(event, branchAddress, index) {
        event.preventDefault();
        this.dialogRef = this.dialog.open(AddressDialogComponent, {
            panelClass: 'form-dialog',
            data: {
                action: 'saveAddress',
                id: branchAddress.id,
                selectedCountryId: branchAddress.country.id,
                selectedStateId: branchAddress.state.id,
                selectedCityId: branchAddress.city.id,
                address: branchAddress.address,
                zip: branchAddress.zip,
                dialogRef: this.dialogRef,
                primary: branchAddress.primary,
                complement: branchAddress.complement,
                hash: branchAddress.hash
            }
        });
        this.onCloseDialogAddress();
    }

}