import { Inject, Injectable, InjectionToken, Optional, AfterViewInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Platform } from '@angular/cdk/platform';
import { UserBusiness } from 'app/service/user/user.service';

export const FUSE_CONFIG = new InjectionToken('fuseCustomConfig');

@Injectable()
export class FuseConfigService implements AfterViewInit {

    config: any;
    defaultConfig: any;

    onConfigChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param router
     * @param platform
     * @param config
     */
    constructor(
        private router: Router,
        public platform: Platform,
        private userBusiness: UserBusiness,
        @Inject(FUSE_CONFIG) @Optional() config
    ) {
        this.buildPage(config, router);
    }

    ngAfterViewInit(): void {
        this.userBusiness.findConfigUser().subscribe(
            success => {
                this.buildPage(success.json(), this.router);
            }
        );
    }

    private buildPage(config: any, router: Router) {
        if (config) {
            this.defaultConfig = config;
        }
        if (this.platform.ANDROID || this.platform.IOS) {
            this.defaultConfig.customScrollbars = false;
        }
        this.config = { ...this.defaultConfig };
        router.events.subscribe((event) => {
            if (event instanceof NavigationStart) {
                this.setConfig({
                    layout: this.config.layout
                });

            }
        });
        this.onConfigChanged = new BehaviorSubject(this.config);
    }

    /**
     * Set the new config from given object
     *
     * @param config
     */
    setConfig(config): void {
        this.config = {
            ...this.config,
            ...config,
            layout: {
                ...this.config.layout,
                ...config.layout,
            },
            colorClasses: {
                ...this.config.colorClasses,
                ...config.colorClasses
            }
        };

        // Trigger the event
        this.onConfigChanged.next(this.config);
    }
}

