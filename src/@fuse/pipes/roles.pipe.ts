import { Pipe, PipeTransform } from '@angular/core';
import { RoleProfile } from 'app/model/profile/role.profile.model';

@Pipe({
    name: 'role',
    pure: false
})
export class RolePipe implements PipeTransform {
    transform(items: RoleProfile[], filter: Object): any {
        if (!items || !filter) {
            return items;
        }
        return items.filter(item => item.roleName.indexOf(filter.toString()) !== -1);
    }
}