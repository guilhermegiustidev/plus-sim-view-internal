import { Pipe, PipeTransform } from '@angular/core';
import { LogicInstance } from '@fuse/utils/logic.instance';

@Pipe({
    name: 'active',
    pure: false
})
export class ActivePipe implements PipeTransform {
    transform(items: LogicInstance[], filter: Object): any {
        if (!items || !filter) {
            return items;
        }
        return items.filter(item => item.status);
    }
}