import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthenticationBusiness } from 'app/service/authentication/auth.service';
import { TranslateService } from '@ngx-translate/core';

declare var swal:any;

@Injectable()
export class PermissionGuard implements CanActivate {

    constructor(private router: Router, private authBusiness: AuthenticationBusiness, private translate: TranslateService) {

    }


    canActivate(
            next: ActivatedRouteSnapshot, 
            state: RouterStateSnapshot) {
        let url = state.url.split('/');
        let module = url[1];
        let program = url[2];

        this.authBusiness.hasPermission(program).subscribe(
            success => {
                if(!success){
                    this.translate.get('PERMISSION.HAS_NO_PERMISSION.TITLE').subscribe(
                        title => {
                            this.translate.get('PERMISSION.HAS_NO_PERMISSION.BODY').subscribe(
                                description => {
                                    swal(title, description, "error", {
                                        button: "Ok",
                                    }).then((value) => {
                                        this.router.navigate(['/']);
                                    });
                                }
                            );
                        }
                    );
                }
            }
        );

        return true;
    }

}