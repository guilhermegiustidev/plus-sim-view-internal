import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthenticationBusiness } from 'app/service/authentication/auth.service';

@Injectable()
export class LoggedGuard implements CanActivate {

    constructor(private router: Router, private authService: AuthenticationBusiness) {
    }


    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let user;
        this.authService.findUser().subscribe(
            success => {
                user = success.json();
                this.authService.refreshToken(user.login).subscribe(
                    success => {
                        let new_access_token = success.json().access_token;
                        localStorage.removeItem('accessToken');
                        localStorage.setItem('accessToken', new_access_token);
                    }, error => {
                        console.log('Error');
                    }
                );
            }, error => {
                console.log('Error');
            }
        );

        if (localStorage.getItem('accessToken')) {
            return true;
        } else {
            this.router.navigate(['/pages/auth/login']);
            return true;
        }

    }

}