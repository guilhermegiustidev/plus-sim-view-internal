import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';

import { AutocompleteModule } from '@fuse/components/autocomplete/autocomplete.module';

import { FuseDirectivesModule } from '@fuse/directives/directives';
import { FusePipesModule } from '@fuse/pipes/pipes.module';

import { LoggedGuard } from './guard/looged.guard';
import { MaterialModule } from './material.module';

import { AuthorizationHandler } from 'app/service/handler/authorization.handler';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AddressModule } from '@fuse/components/address/address.module';
import { ContactModule } from '@fuse/components/contact/contact.module';

import { PermissionGuard } from '@fuse/guard/permission.menu.guard';
import { AutocompleteUserComponent } from '@fuse/components/autocomplete/users/autocomplete.user.component';
import { CityComponent } from '@fuse/components/city/city.component';

@NgModule({
    
    declarations: [
        CityComponent
    ],
    
    imports  : [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MaterialModule,
        FuseDirectivesModule,
        FusePipesModule,
        TranslateModule,
        NgxDatatableModule,
        AddressModule,
        ContactModule,
        AutocompleteModule
    ],
    exports  : [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        TranslateModule,
        FuseDirectivesModule,
        FusePipesModule,
        NgxDatatableModule,
        AddressModule,
        ContactModule,
        AutocompleteModule,
        CityComponent
    ],
    providers   : [
        LoggedGuard,
        AuthorizationHandler,
        PermissionGuard
    ]
})
export class FuseSharedModule
{
}
