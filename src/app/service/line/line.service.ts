import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';
import { Observable } from 'rxjs/Rx';
import { LineModel } from '../../model/line.model';


@Injectable()
export class LineBusiness extends CrudService<LineModel> {
    
    public getPathService(): string {
        return "/line"  
    }
    

}