import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';
import { BranchModel } from 'app/model/branch/branch.model';
import { UserModel } from 'app/model/user.model';
import { ContactModel } from 'app/model/contact.model';
import { AddressComponent } from '@fuse/components/address/address.component';
import { AddressModel } from 'app/model/address.model';


@Injectable()
export class BranchBusiness extends CrudService<BranchModel> {
    
    public getPathService(): string {
        return "/branch"  
    }

    merge(entity: any) {
        return super.post(this.getFullUrl().concat('/mergeBranch'), entity)
        .catch((error: any) => this.handleError(error, this.translate));
    }


    public findUsersBranch(parameter: number) {
        let url: string = this.getFullUrl().concat('/findUserBranch?branchId=' + parameter);
        return this.findInService(url).map(res => <Array<UserModel>>res.json());
    }

    public findContactsBranch(parameter: number) {
        let url: string = this.getFullUrl().concat('/findContactBranch?branchId=' + parameter);
        return this.findInService(url).map(res => <Array<ContactModel>>res.json());
    }

    public findAddressesBranch(parameter: number) {
        let url: string = this.getFullUrl().concat('/findAddressBranch?branchId=' + parameter);
        return this.findInService(url).map(res => <Array<AddressModel>>res.json());
    }




}