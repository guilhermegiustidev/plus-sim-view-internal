import { Injectable, Injector } from '@angular/core';
//import { service_url } from '../../../router'
import { Router } from '@angular/router';

import { Page } from '@fuse/components/page/page';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { server_url } from '@fuse/config/server.config';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { TransferState } from '@angular/platform-browser/src/browser/transfer_state';
declare var swal:any;


@Injectable()
export abstract class Service<T> {

    protected headers: Headers = new Headers();
    public abstract getPathService(): string;

    constructor(
        protected http: Http,
        private router: Router,
        private domSanitizationService: DomSanitizer,
        protected translate: TranslateService
    ) {
    }

    protected prepareHeader(): Headers {
        let userLocal: any = localStorage.getItem('accessToken');
        let headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "bearer " + userLocal,
        });
        return headers;
    }

    protected getFullUrl() {
        return this.getServiceUrl().concat(this.getPathService());
    }

    public getServiceUrl(): string {
        return server_url;
    }

    public findAll(): Observable<Array<T>> {
        return this.findInService(this.getFullUrl().concat('/findAll'))
            .map(res => <Array<T>>res.json())
            .catch((error: any) => this.handleError(error, this.translate));
    }

    public page(entity: T, pageSize: number, pageNumber: number, count: number){
        return this.post(this.getFullUrl().concat('/page?pageSize=' + pageSize + '&pageNumber=' + pageNumber + '&count=' + count), entity).map(res => <Page<T>>res.json())
        .catch((error: any) => this.handleError(error, this.translate));
    }       


    public count(entity: T){
        return this.post(this.getFullUrl().concat('/count'), entity).map(res => <number>res.json())
        .catch((error: any) => this.handleError(error, this.translate));
    }    

    public findById(parameter: number) {
        let url: string = this.getFullUrl().concat('/findById?id=' + parameter);
        return this.findInService(url).map(res => <T>res.json());
    }

    public findInService(url: string) {
        let userLocal: any = localStorage.getItem('accessToken');
        this.headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "bearer " + userLocal,
        });
        return this.http.get(url, { headers: this.headers }).catch(
            (error: any) => this.handleError(error, this.translate)
        );
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }

    protected handleError(error: Response, translate: TranslateService) {
        let result = error.json();
        if (error.status === 403) {
            translate.get('LABELS.ERROR.PERMISSION.TITLE').subscribe(
                title=>{
                    translate.get('LABELS.ERROR.PERMISSION.MESSAGE').subscribe(
                        message => {
                            swal(title, message, 'warning')
                        }
                    );
                }
            )
        }
        return Observable.throw(error.json().error || 'Server Error');
    }


    protected post(_url: string, parameter: any): Observable<Response> {
        let userLocal: any = localStorage.getItem('accessToken');
        let _headers: Headers = new Headers({
            "Content-Type": "application/json",
            "Authorization": "bearer " + userLocal,
        });
        return this.http.post(_url, JSON.stringify(parameter), { headers: _headers });
    }

    protected delete(_url: string): Observable<Response> {
        let userLocal: any = localStorage.getItem('accessToken');
        let _headers: Headers = new Headers({
            "Content-Type": "application/json",
            "Authorization": "bearer " + userLocal,
        });
        return this.http.delete(_url, { headers: _headers });
    }


    protected get(_url: string) {
        let url: string = this.getServiceUrl().concat(this.getPathService()).concat(_url);
        return this.http.get(url).map(res => <Array<T>>res.json()).catch((error: any) => this.handleError(error, this.translate));
    }



}