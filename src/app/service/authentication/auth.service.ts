import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import { Base64 } from 'js-base64';
import { DomSanitizer } from '@angular/platform-browser';
import { CookieService } from 'ngx-cookie-service';
import { server_url } from '@fuse/config/server.config';
import { AuthorizationHandler } from 'app/service/handler/authorization.handler';

@Injectable()
export class AuthenticationBusiness {

    options: RequestOptions;
    protected headers: Headers = new Headers();

    protected getFullUrl(){
        return server_url.concat(this.getPathService());
    }

    constructor(
        protected http: Http, 
        private domSanitizationService: DomSanitizer, 
        private cookieService: CookieService,
        private authorizationHandler: AuthorizationHandler
    ) {
        this.headers.append('Content-Type', 'application/json');
    }

    public getUserKey(): string{
        return 'accessToken';
    }

    public getPathService(): string {
        return "/auth";
    }

    public hasPermission(program: string): Observable<boolean>{
        let userLocal: any = localStorage.getItem('accessToken');

        let _headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "bearer " + userLocal,
        });

        let url: string = server_url.concat('/tokens/hasPermission?programCod=' + program);
        return this.http.get(url, { headers: _headers }).map(res => <boolean>res.json())
            .catch(this.authorizationHandler.handler);
    }

    public isLogged(): boolean {
        return localStorage.getItem(this.getUserKey()) != null;
    }

    public rememberPassword(parameters): Observable<Response> {
        let url: string = server_url.concat(this.getPathService()).concat('/lostEmail').concat('?email=').concat(parameters);
        return this.http.post(url, { headers: this.headers });
    }

    public login(parameters): Observable<Response> {
        this.headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Basic cGx1c19zaW06cGx1NXMxbTRwcGwxYzR0MTBu",
        });
  
        this.options = new RequestOptions({ headers: this.headers });
        let urlSearchParams = new URLSearchParams();
        urlSearchParams.append('username', parameters.username);
        urlSearchParams.append('password', parameters.password);
        urlSearchParams.append('grant_type', "password");
        //urlSearchParams.append('client', "enygma_front");
        return this.http.post(server_url.concat('/oauth/token'), urlSearchParams, this.options);
    }

    public logout(): Observable<Response> {
        let userLocal: any = localStorage.getItem('accessToken');
        this.headers = new Headers({
            "Authorization": "bearer " + userLocal,
        });
        return this.http.delete(server_url.concat('/tokens/revoke'), {headers : this.headers});
    }

    public refreshToken(username){
        
        let _headers: Headers = new Headers({
            "Authorization": "Basic cGx1c19zaW06cGx1NXMxbTRwcGwxYzR0MTBu",
        });

        let userLocal: any = localStorage.getItem('accessToken');

        this.options = new RequestOptions({ headers: _headers});
        let urlSearchParams = new URLSearchParams();
        urlSearchParams.append('username', username);
        urlSearchParams.append('access_token', userLocal);
        urlSearchParams.append('grant_type', 'refresh_token');
        return this.http.post(server_url.concat('/oauth/token'), urlSearchParams, this.options);

    }

    public findMenus(): Observable<Response> {
        let userLocal: any = localStorage.getItem('accessToken');

        let _headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "bearer " + userLocal,
        });

        let url: string = server_url.concat('/tokens/findMenus');
        return this.http.get(url, { headers: _headers }).catch(this.authorizationHandler.handler);
    }

    public findUser(): Observable<Response> {
        let userLocal: any = localStorage.getItem('accessToken');

        let _headers: Headers = new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "bearer " + userLocal,
        });

        let url: string = server_url.concat('/tokens/findUser');
        return this.http.get(url, { headers: _headers }).catch(this.authorizationHandler.handler);
    }



}