import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';
import { AddressModel } from 'app/model/address.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AddressBusiness extends CrudService<AddressModel> {
    
    public getPathService(): string {
        return "/address"  
    }


}