import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';
import { Observable } from 'rxjs/Rx';
import { Page } from '@fuse/components/page/page';
import { Pageable } from '@fuse/components/page/pageable';

import { CityModel } from 'app/model/city.model';
import { StateModel } from 'app/model/state.model';

@Injectable()
export class CityBusiness extends CrudService<CityModel> {
    
    public getPathService(): string {
        return "/location/city"  
    }

    public findCitiesByIdState(stateId: number): Observable<CityModel[]> {
        let url: string = this.getFullUrl().concat('/findCitiesByStateId?stateId=').concat(stateId.toString());
        return super.findInService(url).map(res => <CityModel[]>res.json());
    }
    
    public findCitiesByCountryIdAndLikeNameAndStateCount(countryId: number, name: string, stateId: number){
        let url = '/findCitiesByCountryIdAndLikeNameAndStateCount';

        let parameter = {
            countryId   : countryId,
            stateId       : stateId,
            name        : name,
        }

        return this.post(this.getFullUrl()
        .concat(url), parameter).map(res => <number>res.json())
        .catch((error: any) => this.handleError(error, this.translate));
    }

    public findCitiesByCountryIdAndLikeNameAndStatePage(countryId: number, name: string, stateId: number, pageSize: number, pageNumber: number, count: number) {
        let url = '/findCitiesByCountryIdAndLikeNameAndStatePage';

        let parameter = {
            countryId   : countryId,
            name        : name,
            stateId     : stateId,
            pageSize    : pageSize,
            pageNumber  : pageNumber,
            count       : count
        }
        return this.post(this.getFullUrl()
        .concat(url), parameter).map(res => <Page<CityModel>>res.json())
        .catch((error: any) => this.handleError(error, this.translate));
    }
    
}