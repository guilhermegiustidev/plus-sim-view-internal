import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';
import { Observable } from 'rxjs/Rx';

import { StateModel } from 'app/model/state.model';

@Injectable()
export class StateBusiness extends CrudService<StateModel> {
    
    public getPathService(): string {
        return "/location/state"  
    }


    public findStatesByCountryId(countryId: number): Observable<StateModel[]> {
        let url: string = this.getFullUrl().concat('/findStatesByCountryId?countryId=').concat(countryId.toString());
        return super.findInService(url).map(res => <StateModel[]>res.json());
    }
    

}