import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';

import { CountryModel } from 'app/model/country.model';

@Injectable()
export class CountryBusiness extends CrudService<CountryModel> {
    
    public getPathService(): string {
        return "/location/country"  
    }

}