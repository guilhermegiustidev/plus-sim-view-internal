import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';
import { TypeServicePlanModel } from '../../model/plan/type.service.plan.model';

@Injectable()
export class TypeServicePlanBusiness extends CrudService<TypeServicePlanModel> {
    
    public getPathService(): string {
        return "/planServiceType"  
    }

    public findByLikeName(name: string){
        let url: string = this.getFullUrl().concat('/findByLikeName?name=').concat(name.toString());
        return super.findInService(url).map(res => <Array<TypeServicePlanModel>>res.json());
    }

}