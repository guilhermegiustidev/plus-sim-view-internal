import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';
import { PlanModel } from 'app/model/plan/plan.model';

@Injectable()
export class PlanBusiness extends CrudService<PlanModel> {
    
    public getPathService(): string {
        return "/plan"  
    }

}