import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';
import { ContactModel } from 'app/model/contact.model';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class ContactBusiness extends CrudService<ContactModel> {
    
    public getPathService(): string {
        return "/contact"  
    }


    public findContactTypes() : Observable<Array<string>>{
        let url = '/findContactTypes';
        return super.findInService(this.getFullUrl().concat(url))
        .map(res => <Array<string>>res.json())
        .catch((error: any) => this.handleError(error, this.translate));
    }

}