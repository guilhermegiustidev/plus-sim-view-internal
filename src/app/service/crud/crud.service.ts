import { Inject } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

import { Service } from 'app/service/client.service';
import { TranslateService } from '@ngx-translate/core';

declare var swal: any;

export abstract class CrudService<T> extends Service<T> {

    merge(entity: T) {
        return super.post(this.getFullUrl().concat('/merge'), entity)
            .map(res => <T>res.json())
            .catch((error: any) => this.handleError(error, this.translate));
    }

    public getHash() : Observable<string>{
        return super.findInService(this.getFullUrl().concat('/hash'))
        .catch((error: any) => this.handleError(error, this.translate));
    }

    removeAll(entity: T[]) : Promise<any>{
        return new Promise((resolve, reject) => {
            Promise.all([
                this.translate.get('CONFIRM.EXCLUDE.TITLE'),
                this.translate.get('CONFIRM.EXCLUDE.BODY'),
                this.translate.get('CONFIRM.EXCLUDE.YES'),
                this.translate.get('CONFIRM.EXCLUDE.NO'),
                this.translate.get('ACTION.TITLE.EXCLUDE'),
                this.translate.get('ACTION.BODY.EXCLUDE'),
                this.translate.get('CONFIRM.EXCLUDE.ERROR.TITLE'),
                this.translate.get('CONFIRM.EXCLUDE.ERROR.BODY'),
            ]).then(
                (success) => {
                    swal(success[0]['value'], success[1]['value'], {
                        buttons: {
                            no: {
                                text: success[3]['value'],
                                value: "no",
                            },
                            yes: {
                                text: success[2]['value'],
                                value: "yes",
                            },
                        },
                        showLoaderOnConfirm: true
                    }).then((value) => {
                        switch (value) {
                            case "yes":
                                super.post(this.getFullUrl().concat('/deleteAll'), entity)
                                    .catch(
                                        (error: any) => this.handleError(error, this.translate)
                                    ).subscribe(
                                        remove => {
                                            swal(
                                                success[4]['value'],
                                                success[5]['value'],
                                                "success")
                                                resolve();
                                        }, error =>{
                                            swal(
                                                success[6]['value'],
                                                success[7]['value'],
                                                "error")
                                            resolve();
                                        }
                                    );
                                break;
                            default:
                                resolve();
                            break;
                        }
                    });

                },
                reject
            );
        });

    }

    remove(entity: T) {
        let url = this.getFullUrl().concat('/delete?id=' + entity['id']);
        return super.delete(url).catch((error: any) => this.handleError(error, this.translate));
    }

    page(entity: T, pageSize: number, pageNumber: number, count: number) {
        return super.page(entity, pageSize, pageNumber, count);
    }

    count(entity: T) {
        return super.count(entity);
    }

} 