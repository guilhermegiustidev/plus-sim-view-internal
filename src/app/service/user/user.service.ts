import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';

import { UserModel } from 'app/model/user.model';
import { BranchModel } from 'app/model/branch/branch.model';
import { PartnerModel } from 'app/model/partner/partner.model';
import { ProfileModel } from 'app/model/profile/profile.model';
import { ContactModel } from 'app/model/contact.model';

@Injectable()
export class UserBusiness extends CrudService<UserModel> {

    public getPathService(): string {
        return "/person"
    }

    merge(entity: any) {
        return super.post(this.getFullUrl().concat('/mergePerson'), entity)
        .catch((error: any) => this.handleError(error, this.translate));
    }

    mergeMyAccount(entity: any): any {
        return super.post(this.getFullUrl().concat('/mergeMyAccountData'), entity)
        .catch((error: any) => this.handleError(error, this.translate));
    }

    findConfigUser(){
        return super.get(this.getFullUrl().concat('/findConfig'))
        .catch((error: any) => this.handleError(error, this.translate));
    }

    verifyLogin(arg0: any): any {
        let url = '/verifyLogin';
        if(arg0){
            url+='?login=' + arg0;
        }
        return super.findInService(this.getFullUrl().concat(url))
            .map(res => <boolean>res.json())
            .catch((error: any) => this.handleError(error, this.translate));
    }

    findByLikeName(name: string): any {
        let url = '/findByLikeName';
        if(name){
            url+='?name=' + name;
        }
        return super.findInService(this.getFullUrl().concat(url))
            .map(res => <Array<UserModel>>res.json())
            .catch((error: any) => this.handleError(error, this.translate));
    }

    findBranchsUser(userId: number): any {
        let url = '/findBranchsUser';
        if(userId){
            url+='?personId=' + userId;
        }
        return super.findInService(this.getFullUrl().concat(url))
            .map(res => <Array<BranchModel>>res.json())
            .catch((error: any) => this.handleError(error, this.translate));
    }

    findPartnersUser(userId: number): any {
        let url = '/findPartnersUser';
        if(userId){
            url+='?personId=' + userId;
        }
        return super.findInService(this.getFullUrl().concat(url))
            .map(res => <Array<PartnerModel>>res.json())
            .catch((error: any) => this.handleError(error, this.translate));
    }

    findProfilesUser(userId: number): any {
        let url = '/findProfilesUser';
        if(userId){
            url+='?personId=' + userId;
        }
        return super.findInService(this.getFullUrl().concat(url))
            .map(res => <Array<ProfileModel>>res.json())
            .catch((error: any) => this.handleError(error, this.translate));
    }

    findContactUser(userId: number): any {
        let url = '/findContactUser';
        if(userId){
            url+='?personId=' + userId;
        }
        return super.findInService(this.getFullUrl().concat(url))
            .map(res => <Array<ContactModel>>res.json())
            .catch((error: any) => this.handleError(error, this.translate));
    }

    

}