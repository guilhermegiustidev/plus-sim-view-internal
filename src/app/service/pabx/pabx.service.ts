import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';
import { PabxModel } from '../../model/pabx/pabx.model';

@Injectable()
export class PabxBusiness extends CrudService<PabxModel> {
    
    public getPathService(): string {
        return "/pabx"  
    }
}