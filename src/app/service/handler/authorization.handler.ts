import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Response } from '@angular/http';
import { Router } from '@angular/router';



@Injectable()
export class AuthorizationHandler {

    constructor(private router: Router){

    }

    handler(error: Response) {
        let result = error.json();
        if(result.error === 'unauthorized'){
            console.log('TESTE');
        }
        if(result.error === 'access_denied'){
            console.log('Acesso Negado');
        }
        if(result.error === 'invalid_token'){
            console.log('INVALIDO')
        }
        return Observable.throw(error.json().error || 'Server Error');
    }


}