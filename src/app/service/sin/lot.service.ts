import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';

import { Observable } from 'rxjs/Observable';
import { LotModel } from '../../model/sin/lot.model';


@Injectable()
export class LotService extends CrudService<LotModel> {
    
    public getPathService(): string {
        return "/lot"  
    }

    generateLot(status: string): Observable<LotModel> {
        let url = '/generateLot?status=' + status;
        return super.findInService(this.getFullUrl().concat(url)).map(res => <LotModel>res.json())
            .catch((error: any) => this.handleError(error, this.translate));
    }

    verifyProgress(): Observable<LotModel> {
        let url = '/verifyProgress';
        return super.findInService(this.getFullUrl().concat(url)).map(res => <boolean>res.json())
            .catch((error: any) => this.handleError(error, this.translate));
    }

}