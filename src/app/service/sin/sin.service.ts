import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';

import { Observable } from 'rxjs/Observable';
import { LotModel } from '../../model/sin/lot.model';
import { SinModel } from '../../model/sin/sin.model';


@Injectable()
export class SinService extends CrudService<SinModel> {
    
    public getPathService(): string {
        return "/sin"  
    }

    public addSin(providerId: number, code: string, lotId: number){
        let url: string = this.getFullUrl().concat('/addSin?lotId=' + lotId + '&providerId=' + providerId + '&code=' + code);
        return super.findInService(url);
    }
}