import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';

import { ProfileModel } from 'app/model/profile/profile.model';
import { RoleProfile } from 'app/model/profile/role.profile.model';
import { Observable } from 'rxjs/Observable';
import { UserProfile } from 'app/model/profile/user.profile.model';
import { ModuleProfileModel } from '../../model/profile/module.profile.model';


@Injectable()
export class ProfileBusiness extends CrudService<ProfileModel> {
    
    public getPathService(): string {
        return "/profile"  
    }

    merge(entity: any) {
        return super.post(this.getFullUrl().concat('/mergeProfile'), entity)
        .catch((error: any) => this.handleError(error, this.translate));
    }

    findByLikeName(name: any): any {
        let url = '/findByLikeName';
        if(name){
            url+='?name=' + name;
        }
        return super.findInService(this.getFullUrl().concat(url))
            .map(res => <Array<ProfileModel>>res.json())
            .catch((error: any) => this.handleError(error, this.translate));
    }

    public findRolesProfile(profileId: number) : Observable<Array<RoleProfile>>{
        let url = '/findAllRoles';
        if(profileId){
            url += '?idProfile=' + profileId;
        }
        return super.findInService(this.getFullUrl().concat(url))
        .map(res => <Array<RoleProfile>>res.json())
        .catch((error: any) => this.handleError(error, this.translate));
    }

    public findAllUsers(profileId: number) : Observable<Array<UserProfile>>{
        let url = '/findAllUsers';
        if(profileId){
            url += '?idProfile=' + profileId;
        }
        return super.findInService(this.getFullUrl().concat(url))
        .map(res => <Array<UserProfile>>res.json())
        .catch((error: any) => this.handleError(error, this.translate));
    }

    public findAllPrograms(profileId: number) : Observable<Array<ModuleProfileModel>>{
        let url = '/findAllPrograms';
        if(profileId){
            url += '?idProfile=' + profileId;
        }
        return super.findInService(this.getFullUrl().concat(url))
        .map(res => <Array<ModuleProfileModel>>res.json())
        .catch((error: any) => this.handleError(error, this.translate));
    }
    

    
}