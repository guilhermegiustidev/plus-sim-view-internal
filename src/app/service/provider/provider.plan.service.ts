import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';

import { Observable } from 'rxjs/Observable';
import { ProviderPlanModel } from 'app/model/provider/providerplan.model';


@Injectable()
export class ProviderPlanBusiness extends CrudService<ProviderPlanModel> {
    
    public getPathService(): string {
        return "/planProvider"  
    }
    

    public findPlanProviderByProviderId(providerId: number){
        let url: string = this.getFullUrl().concat('/findPlanProviderByProviderId?id=').concat(providerId.toString());
        return super.findInService(url).map(res => <Array<ProviderPlanModel>>res.json());
    }
}