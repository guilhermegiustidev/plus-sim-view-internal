import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';

import { Observable } from 'rxjs/Observable';
import { ProviderModel } from 'app/model/provider/provider.model';


@Injectable()
export class ProviderBusiness extends CrudService<ProviderModel> {
    
    public getPathService(): string {
        return "/provider"  
    }

}