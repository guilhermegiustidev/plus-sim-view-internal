import { Injectable } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';
import { PartnerModel } from 'app/model/partner/partner.model';
import { UserModel } from 'app/model/user.model';
import { ContactModel } from 'app/model/contact.model';
import { AddressModel } from 'app/model/address.model';

@Injectable()
export class PartnerBusiness extends CrudService<PartnerModel> {
    
    public getPathService(): string {
        return "/partner"  
    }

    merge(entity: any) {
        return super.post(this.getFullUrl().concat('/mergePartner'), entity)
        .catch((error: any) => this.handleError(error, this.translate));
    }
    
    public findUserPartner(parameter: number) {
        let url: string = this.getFullUrl().concat('/findUserPartner?partnerId=' + parameter);
        return this.findInService(url).map(res => <Array<UserModel>>res.json());
    }

    public findContactPartner(parameter: number) {
        let url: string = this.getFullUrl().concat('/findContactPartner?partnerId=' + parameter);
        return this.findInService(url).map(res => <Array<ContactModel>>res.json());
    }

    public findAddressPartner(parameter: number) {
        let url: string = this.getFullUrl().concat('/findAddressPartner?partnerId=' + parameter);
        return this.findInService(url).map(res => <Array<AddressModel>>res.json());
    }

}