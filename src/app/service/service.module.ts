import { NgModule, Injector } from '@angular/core';
import { CommonModule } from '@angular/common'
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';

import { AuthenticationBusiness } from './authentication/auth.service';

import { UserBusiness } from './user/user.service';
import { BranchBusiness } from './branch/branch.service';
import { CityBusiness } from './location/city.service';
import { StateBusiness } from './location/state.service';
import { CountryBusiness } from './location/country.service';
import { PartnerBusiness } from './partner/partner.service';
import { AddressBusiness } from './address/address.service';
import { ProfileBusiness } from 'app/service/profile/profile.service';
import { ContactBusiness } from './contact/contact.service';
import { PlanBusiness } from 'app/service/plan/plan.service';
import { LineBusiness } from 'app/service/line/line.service';
import { PabxBusiness } from 'app/service/pabx/pabx.service';
import { ProviderBusiness } from 'app/service/provider/provider.service';
import { ProviderPlanBusiness } from 'app/service/provider/provider.plan.service';
import { TypeServicePlanBusiness } from 'app/service/plan/type.service.plan.service';
import { SinService } from 'app/service/sin/sin.service';
import { LotService } from 'app/service/sin/lot.service';

@NgModule({
    imports: [
        CommonModule, HttpModule
    ],
    providers: [
        AuthenticationBusiness,
        UserBusiness,
        ContactBusiness, 
        BranchBusiness,
        CityBusiness,
        StateBusiness,
        CountryBusiness,
        PartnerBusiness,
        AddressBusiness,
        ProfileBusiness,
        PlanBusiness,
        LineBusiness,
        PabxBusiness,
        ProviderBusiness,
        ProviderPlanBusiness,
        TypeServicePlanBusiness,
        SinService,
        LotService
    ],
})
export class ServiceModule {
}
