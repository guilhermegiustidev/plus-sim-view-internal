import { CountryModel } from './country.model';
import { StateModel } from './state.model';
import { CityModel } from './city.model';
import { LogicInstance } from '@fuse/utils/logic.instance';

export class AddressModel implements LogicInstance{
    id          : number;
    address     : string;
	country     : CountryModel;
	state       : StateModel;
	city        : CityModel;
    zip			: string;
    primary     : boolean;
    status      : boolean;
    complement  : string;
    hash        : string;
}