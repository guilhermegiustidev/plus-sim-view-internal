import { Language } from "@fuse/language/language.component";

export class ContactModel {
	id          : number;
	name   		: string;
	type		: string;
	email		: string;
	phone		: string;
	hash		: string;
	langs		: Array<Language>;
	primary		: boolean;
	status		: boolean;
}