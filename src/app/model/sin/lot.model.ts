import { SinModel } from "app/model/sin/sin.model";

export interface LotModel {
    id: number;
    code: string;
    sins: Array<SinModel>;
    status: string;
}