import { LotModel } from "./lot.model";
import { ProviderModel } from "app/model/provider/provider.model";

export interface SinModel {
    id: number;
    code: string;
    lot: LotModel;   
    provider: ProviderModel;
}