import { ProviderModel } from "app/model/provider/provider.model";
import { TypeServicePlanModel } from "app/model/plan/type.service.plan.model";

export interface ProviderPlanModel {
    id: number;
    name: string;
    status: boolean;
    cost: number;
    provider: ProviderModel;
    services: Array<TypeServicePlanModel>;
}