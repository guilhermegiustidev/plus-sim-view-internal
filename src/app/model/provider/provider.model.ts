import { ContactModel } from '../contact.model';

export interface ProviderModel{
	id          : number;
	name        : string;
	status      : boolean;
	contacts    : ContactModel[];
	regex		: string;
}