import { CountryModel } from 'app/model/country.model';
import { CityModel } from 'app/model/city.model';

export interface PabxModel {
	id : number;
	numberLine : string;
	status : boolean;
	country : CountryModel;
	city : CityModel;
	statusEnum: string;
}