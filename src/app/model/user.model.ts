
export class UserModel {
    id: number;
    name: string;
    email:string;
    login:string;
    status:boolean;
    facebook:string;
    avatar: any;
    config: any;
}