
export class ProfileModel {
    id          : number;
    description : string;
    status      : boolean;
}