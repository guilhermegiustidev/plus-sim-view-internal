import { LogicInstance } from "@fuse/utils/logic.instance";

export class UserProfile implements LogicInstance {
    idUser      : number;
	idProfile   : number;
	status      : boolean;
	userName    : string;
}