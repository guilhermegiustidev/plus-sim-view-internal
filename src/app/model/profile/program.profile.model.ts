export interface ProgramProfileModel {

	id : string;
	title : string;
	translate : string;
    status : boolean;
}