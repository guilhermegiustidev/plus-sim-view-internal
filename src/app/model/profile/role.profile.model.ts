export interface RoleProfile {
    id      : number;
	idProfile   : number;
	status      : boolean;
	roleName    : string;
}