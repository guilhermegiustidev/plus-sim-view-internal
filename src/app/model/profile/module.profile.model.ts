import { ProgramProfileModel } from "./program.profile.model";

export interface ModuleProfileModel {

	id : string;
	title : string;
	translate : string;
    children : ProgramProfileModel[];
}