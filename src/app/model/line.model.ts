import { CountryModel } from './country.model';
import { CityModel } from 'app/model/city.model';
import { ProviderModel } from 'app/model/provider/provider.model';
import { ProviderPlanModel } from 'app/model/provider/providerplan.model';

export interface LineModel {

	id          	: number;
	number      	: string;
	status      	: string;
	released    	: Date;
	country     	: CountryModel;
	city			: CityModel;
	provider		: ProviderModel;
	providerPlan 	: ProviderPlanModel
}