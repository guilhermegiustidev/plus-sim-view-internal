import { BranchModel } from './branch.model';
import { AddressModel } from '../address.model';

export class BranchAddress {
    id      : number;
    branch  : BranchModel;
    address : AddressModel;
    primary : boolean;
}