import { BranchAddress } from './branch.address.model';
import { BranchContact } from './branch.contact.model';

export class BranchModel {

	id          		: number;
	name   				: string;
	branchAddress		: Array<BranchAddress>;
	branchContact		: Array<BranchContact>;
	status				: boolean;
}