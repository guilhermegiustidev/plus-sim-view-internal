import { BranchModel } from './branch.model';
import { AddressModel } from '../address.model';
import { ContactModel } from '../contact.model';

export class BranchContact {
    id          : number;
    contact     : ContactModel;
    primary     : boolean;
    status      : boolean;
}