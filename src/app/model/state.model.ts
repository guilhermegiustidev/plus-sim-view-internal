import { CountryModel } from "app/model/country.model";

export class StateModel {

	id: number;
	name: string;
	code: string;
	country: CountryModel;
}