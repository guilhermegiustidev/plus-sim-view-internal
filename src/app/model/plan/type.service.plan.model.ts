export interface TypeServicePlanModel {
    id: number;
    name: string;
    status: boolean;
}