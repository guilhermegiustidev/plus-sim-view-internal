import { PostLoad } from "app/rules/postload.component";
import { TypeServicePlanModel } from "app/model/plan/type.service.plan.model";


export class PlanModel implements PostLoad {
    id: number;
    price: number;
    cost: number;
    minutes: number;
    valorProfit: number;
    days: number;
    services: Array<TypeServicePlanModel>
    onPostLoad() {
        this.days = this.minutes / 60
        this.days = this.days / 24;
    }
}