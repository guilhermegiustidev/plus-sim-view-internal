import { StateModel } from "app/model/state.model";

export class CityModel {

	id: number;
	name: string;
	code: string;
	locale: string;
	state: StateModel;
}