import { Component, ContentChild, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

import { locale as english } from '@fuse/components/navigation/i18n/en';
import { locale as portuguese } from '@fuse/components/navigation/i18n/pt';
import { locale as spanish } from '@fuse/components/navigation/i18n/sp';

import { FuseToolbarComponent } from 'app/main/toolbar/toolbar.component';

@Component({
    selector: 'fuse-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(
        private translate: TranslateService,
        private fuseNavigationService: FuseNavigationService,
        private fuseSplashScreen: FuseSplashScreenService,
        private fuseTranslationLoader: FuseTranslationLoaderService
    ) {
        this.translate.addLangs(['en', 'pt', 'sp']);
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
        this.translate.setDefaultLang('en');

    }


}
