import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';

import { fuseConfig } from './fuse-config';


import { AppComponent } from './app.component';
import { FuseMainModule } from './main/main.module';
import { PlusSimDashboardModule } from './main/content/dashboard/dashboard.module';
import { FusePagesModule } from './main/content/pages/pages.module';
import { ServiceModule } from 'app/service/service.module';
import { Component } from '@angular/core/src/metadata/directives';
import { LoggedGuard } from '@fuse/guard/looged.guard';
import { PermissionGuard } from '@fuse/guard/permission.menu.guard';


const appRoutes: Routes = [
    {
        path        : 'pages',
        loadChildren: './main/content/pages/pages.module#FusePagesModule'
    },
    
    {
        path        : 'ADM',
        loadChildren: './main/content/administrator/administrator.module#AdministratorModule',
        canActivate: [LoggedGuard, PermissionGuard]
    },
    
    {
        path        : 'config',
        loadChildren: './main/content/config/config.module#ConfigModule',
        canActivate: [LoggedGuard]
    },

    {
        path      : '**',
        redirectTo: ''
    }
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),
        TranslateModule.forRoot(),

        // Fuse Main and Shared modules
        FuseModule.forRoot(fuseConfig),
        FuseSharedModule,
        FuseMainModule,
        PlusSimDashboardModule,
        ServiceModule
    ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule {
    static injector: Injector;
    constructor(injector: Injector) {
        AppModule.injector = injector;
    }
}
