import { Component } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { AuthenticationBusiness } from 'app/service/authentication/auth.service';
import { navigation } from 'app/navigation/navigation';
import { system_lang } from '@fuse/language/language.component';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { UserModel } from 'app/model/user.model';

@Component({
    selector: 'fuse-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss']
})

export class FuseToolbarComponent {
    userStatusOptions: any[];
    languages: any;
    selectedLanguage: any;
    showLoadingBar: boolean;
    horizontalNav: boolean;
    noNav: boolean;

    user: UserModel;

    username: string;

    constructor(
        private router: Router,
        private fuseConfig: FuseConfigService,
        private sidebarService: FuseSidebarService,
        private translate: TranslateService,
        private authenticationBusiness: AuthenticationBusiness,
        private fuseTranslationLoader: FuseTranslationLoaderService

    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);

        this.authenticationBusiness.findUser().subscribe(
            success => {
                this.user = success.json();
                this.username = this.user.name;
                if(this.user.config){
                    this.fuseConfig.setConfig(this.user.config);
                }
                this.authenticationBusiness.refreshToken(this.user.login).subscribe(
                    success => {
                        let new_access_token = success.json().access_token;
                        localStorage.removeItem('accessToken');
                        localStorage.setItem('accessToken', new_access_token);
                    }, error => {
                        this.authenticationBusiness.logout();
                        localStorage.removeItem('accessToken');
                        this.router.navigate(['/pages/auth/login']);
                    }
                );
            }, error => {
                this.authenticationBusiness.logout();
                localStorage.removeItem('accessToken');
                this.router.navigate(['/pages/auth/login']);
            }
        );

        this.userStatusOptions = [
            {
                'title': 'Online',
                'icon': 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon': 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon': 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon': 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon': 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];

        this.languages = system_lang;

        this.selectedLanguage = this.languages[0];

        router.events.subscribe(
            (event) => {
                if (event instanceof NavigationStart) {
                    this.showLoadingBar = true;
                }
                if (event instanceof NavigationEnd) {
                    this.showLoadingBar = false;
                }
            });

        this.fuseConfig.onConfigChanged.subscribe((settings) => {
            this.horizontalNav = settings.layout.navigation === 'top';
            this.noNav = settings.layout.navigation === 'none';
        });

    }

    toggleSidebarOpened(key) {
        this.sidebarService.getSidebar(key).toggleOpen();
    }

    logout(event) {
        event.preventDefault();
        this.authenticationBusiness.logout();
        this.router.navigate(['/pages/auth/login'])
    }

    search(value) {
        console.log(value);
    }

    setLanguage(lang) {
        this.selectedLanguage = lang;
        this.translate.use(lang.id);
    }
}
