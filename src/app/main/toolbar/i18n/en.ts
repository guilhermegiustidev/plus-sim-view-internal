export const locale = {
    lang: 'en',
    data: {
        'TOOLBAR': {
            'USER' : {
                'MY_ACCOUNT': 'Minha Conta',
                'LOGOUT': 'Sair'
            }
        }
    }
};
