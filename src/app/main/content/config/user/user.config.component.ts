import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { PlanBusiness } from 'app/service/plan/plan.service';

import { UserModel } from 'app/model/user.model';
import { AuthenticationBusiness } from 'app/service/authentication/auth.service';
import { Router } from '@angular/router';
import { ThemeOptionComponent } from 'app/main/content/config/user/config-option/config-option.component';
import { UserBusiness } from 'app/service/user/user.service';

@Component({
    templateUrl: './user.config.component.html',
    styleUrls: ['./user.config.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class UserConfigComponent {

    user: UserModel;
    @ViewChild(ThemeOptionComponent) configLayout: ThemeOptionComponent;

    username: string;
    constructor(
        private router: Router,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private authenticationBusiness: AuthenticationBusiness,
        private userBusiness: UserBusiness
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
        this.authenticationBusiness.findUser().subscribe(
            success => {
                this.user = success.json();
                this.username = this.user.name;
                this.configLayout.config = this.user.config;
                this.authenticationBusiness.refreshToken(this.user.login).subscribe(
                    success => {
                        let new_access_token = success.json().access_token;
                        localStorage.removeItem('accessToken');
                        localStorage.setItem('accessToken', new_access_token);
                    }, error => {
                        this.authenticationBusiness.logout();
                        localStorage.removeItem('accessToken');
                        this.router.navigate(['/pages/auth/login']);
                    }
                );
            }, error => {
                this.authenticationBusiness.logout();
                localStorage.removeItem('accessToken');
                this.router.navigate(['/pages/auth/login']);
            }
        );
    }


    save(event){

    }

}
