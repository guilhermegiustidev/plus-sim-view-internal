import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatOptionModule, MatRadioModule, MatSelectModule, MatSlideToggleModule } from '@angular/material';
import { FuseMaterialColorPickerModule } from '@fuse/components/material-color-picker/material-color-picker.module';

import { ThemeOptionComponent } from './config-option.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        ThemeOptionComponent
    ],
    imports     : [
        CommonModule,
        FormsModule,

        FlexLayoutModule,
        TranslateModule,
        MatButtonModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatOptionModule,
        MatRadioModule,
        MatSelectModule,
        MatSlideToggleModule,

        FuseMaterialColorPickerModule
    ],
    exports     : [
        ThemeOptionComponent
    ]
})
export class UserThemeOptionsModule
{
}
