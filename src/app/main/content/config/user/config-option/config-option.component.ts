import { Component, ElementRef, HostBinding, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { style, animate, AnimationBuilder, AnimationPlayer } from '@angular/animations';
import { Subscription } from 'rxjs/Subscription';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfigService } from '@fuse/services/config.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

import { navigation } from 'app/navigation/navigation';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { UserBusiness } from 'app/service/user/user.service';

@Component({
    selector: 'theme-option',
    templateUrl: './config-option.component.html',
    styleUrls: ['./config-option.component.scss'],
    animations: fuseAnimations
})
export class ThemeOptionComponent implements OnDestroy {
    @ViewChild('openButton') openButton;
    @ViewChild('panel') panel;
    @ViewChild('overlay') overlay: ElementRef;

    public player: AnimationPlayer;
    config: any;

    onConfigChanged: Subscription;

    @HostBinding('class.bar-closed') barClosed: boolean;

    constructor(
        private animationBuilder: AnimationBuilder,
        private fuseConfig: FuseConfigService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private navigationService: FuseNavigationService,
        private userBusines: UserBusiness,
        private renderer: Renderer2
    ) {
        this.barClosed = true;
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);

        this.onConfigChanged =
            this.fuseConfig.onConfigChanged
                .subscribe(
                (newConfig) => {
                    this.config = newConfig;
                });

    }

    ngOnDestroy() {
        this.onConfigChanged.unsubscribe();
    }

    onSettingsChange() {
        let parameter = {
            config: this.config
        }
        this.userBusines.mergeMyAccount(parameter).subscribe(
            success => {
                this.fuseConfig.setConfig(this.config);
            }
        );
    }

}
