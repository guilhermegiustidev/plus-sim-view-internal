export const locale = {
    lang: 'en',
    data: {
        'CONFIG': {
            'USER': {
                'NAME': 'Nome'
            },
            'COLORS': {
                'TITLE': 'Cores',
                'TOOLBAR': 'Barra de ferramentas',
                'NAVIGATION': 'Menu de Navegação',
                'FOOTER': 'Rodapé'
            },
            'LAYOUT': {
                'TITLE': 'Configuração de Layout'
            },
            'NAVIGATION': {
                'TITLE': 'Menu de Navegação',
                'TOP': 'Topo',
                'LEFT': 'Esquerda',
                'RIGHT': 'Direita',
            },
            'NAVIGATION_FOLDER': {
                'TITLE': 'Menu minimizado',
                'YES': 'Sim',
                'NO': 'Não'
            },
            'TOOLBAR' : {
                'TITLE': 'Barra de ferramentas',
                'BELOW': 'Abaixo',
                'ABOVE': 'Acima',
            },
            'FOOTER': {
                'TITLE': 'Rodapé',
                'BELOW': 'Abaixo',
                'ABOVE': 'Acima',
                'NONE': 'Sem Rodapé'
            },

            'HORIZONTAL_SIZE': {
                'TITLE': 'Tamanho Horizontal',
                'FULL': 'Tela Cheia',
                'BOXED': 'Tela Reduzida',
                
            },

            'ACTION': {
                'SAVE': 'Salvar',
                'PHOTO': 'Trocar Foto',
                'APPLY': 'Aplicar Mudanças',
            },
            'TABS': {
                'DATA_ACCOUNT': 'Dados da Conta',
                'DATA_CONFIG': 'Configuração de Layout'
            }
        }
    }
};
