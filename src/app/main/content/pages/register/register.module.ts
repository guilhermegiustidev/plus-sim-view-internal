import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { RegisterUserComponent } from './user/register.user.component';
const routes = [
    {
        path     : 'user',
        component: RegisterUserComponent
    }
];

@NgModule({
    declarations: [
        RegisterUserComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        FuseSharedModule
    ]
})
export class RegisterModule
{
}
