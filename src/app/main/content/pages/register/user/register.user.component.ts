import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

declare var swal:any;

import { locale as english } from './i18n/en';
import { locale as spanish } from './i18n/sp';
import { locale as portuguese } from './i18n/pt';

@Component({
    templateUrl: './register.user.component.html',
    styleUrls: ['./register.user.component.scss'],
    animations: fuseAnimations
})
export class RegisterUserComponent implements OnInit {
    
    registerForm: FormGroup;
    registerFormErrors: any;
    processing: boolean;

    constructor(
        private translate: TranslateService,
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private router: Router
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
        
        this.fuseConfig.setConfig({

        });

        this.registerFormErrors = {
            name: {},
        };
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            name: ['', Validators.required]
        });

        this.registerForm.valueChanges.subscribe(() => {
            this.onregisterFormValuesChanged();
        });
    }

    onregisterFormValuesChanged() {
        for (const field in this.registerFormErrors) {
            if (!this.registerFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.registerFormErrors[field] = {};

            // Get the control
            const control = this.registerForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.registerFormErrors[field] = control.errors;
            }
        }
    }
}
