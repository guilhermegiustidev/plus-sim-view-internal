import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

declare var swal:any;

import { locale as english } from './i18n/en';
import { locale as spanish } from './i18n/sp';
import { locale as portuguese } from './i18n/pt';

import { AuthenticationBusiness } from 'app/service/authentication/auth.service';

@Component({
    selector: 'fuse-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: fuseAnimations
})
export class FuseLoginComponent implements OnInit {
    loginForm: FormGroup;
    loginFormErrors: any;
    username: string;
    password: string;

    processing: boolean;

    constructor(
        private translate: TranslateService,
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private authenticationService: AuthenticationBusiness,
        private router: Router
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
        
        this.fuseConfig.setConfig({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.loginFormErrors = {
            username: {},
            password: {}
        };
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required]],
            password: ['', Validators.required]
        });

        this.loginForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });
    }

    onLoginFormValuesChanged() {
        for (const field in this.loginFormErrors) {
            if (!this.loginFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }
    login(event){
        event.preventDefault();
        this.processing = true;
        this.authenticationService.login({username: this.username, password: this.password}).subscribe(
            success => {
                this.processing = false;
                localStorage.setItem('accessToken', success.json().access_token);
                this.router.navigate(['/'])
            }, error => {
                this.processing = false;
                this.translate.get('LOGIN.LABELS.USER.INVALID.TITLE').subscribe(
                    title => {
                        this.translate.get('LOGIN.LABELS.USER.INVALID.BODY').subscribe(
                            description => {
                                swal(title, description, "error");
                            }
                        );
                    }
                );

            }
        );
    }
}
