export const locale = {
    lang: 'pt',
    data: {
        'LOGIN': {
            'TITLE': 'Bem Vindo à Plus Sim',

            'ACTION': {
                'REMEMBER_ME': 'Manter conectado',
                'FORGET_PASSWORD': 'Esqueceu senha?',
                'LOGIN': 'Login'
            },
            'LABELS': {
                'USER': {
                    'INVALID': {
                        'TITLE': 'Senha e/ou email inválido(s)',
                        'BODY': 'Não possível realizar login'
                    }
                },
                'USERNAME': {
                    'PLACEHOLDER': 'Email',
                    'ERROR': {
                        'REQUIRED': 'Email obrigatório',
                        'INVALID': 'Padrão de email inválido'
                    }
                },
                'PASSWORD': {
                    'PLACEHOLDER': 'Senha',
                    'ERROR': {
                        'REQUIRED': 'Senha obrigatória',
                    }
                }
            }
        }
    }
};
