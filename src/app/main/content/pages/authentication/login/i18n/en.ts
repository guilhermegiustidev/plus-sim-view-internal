export const locale = {
    lang: 'en',
    data: {
        'LOGIN': {
            'TITLE': 'Bem Vindo à Plus Sim',

            'ACTION': {
                'REMEMBER_ME': 'Manter conectado',
                'FORGET_PASSWORD': 'Esqueceu senha?',
                'LOGIN': 'Login'
            },
            'LABELS': {
                'USER': {
                    'INVALID': {
                        'TITLE': 'Senha e/ou Login inválido(s)',
                        'BODY': 'Não possível realizar login'
                    }
                },
                'USERNAME': {
                    'PLACEHOLDER': 'Login',
                    'ERROR': {
                        'REQUIRED': 'Login obrigatório',
                        'INVALID': 'Padrão de Login inválido'
                    }
                },
                'PASSWORD': {
                    'PLACEHOLDER': 'Senha',
                    'ERROR': {
                        'REQUIRED': 'Senha obrigatória',
                    }
                }
            }
        }
    }
};


