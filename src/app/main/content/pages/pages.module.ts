import { NgModule } from '@angular/core';

import { LoginModule } from './authentication/login/login.module';
import { RegisterModule } from './register/register.module';

@NgModule({
    imports: [
        LoginModule,
    ]
})
export class FusePagesModule
{

}
