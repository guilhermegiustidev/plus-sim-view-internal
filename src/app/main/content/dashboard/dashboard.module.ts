import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { PlusSimAdminitrationDashboardComponent } from './administration/dashboard.administration';
import { LoggedGuard } from '@fuse/guard/looged.guard';

const routes = [
    {
        path     : '',
        component: PlusSimAdminitrationDashboardComponent,
        canActivate: [LoggedGuard]
    }
];

@NgModule({
    declarations: [
        PlusSimAdminitrationDashboardComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        FuseSharedModule
    ],

})

export class PlusSimDashboardModule
{
}
