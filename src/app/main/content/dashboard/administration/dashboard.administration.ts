import { Component } from '@angular/core';

import { AuthenticationBusiness } from 'app/service/authentication/auth.service';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
    templateUrl: './dashboard.administration.html',
    styleUrls  : ['./dashboard.administration.scss']
})
export class PlusSimAdminitrationDashboardComponent {


    
    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private authenticationBusiness: AuthenticationBusiness,
        private fuseConfig: FuseConfigService
    ){
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
        this.authenticationBusiness.findUser().subscribe(
            success => {
                let user = success.json();
                this.fuseConfig.setConfig(user.config);
            }
        );
    }
}
