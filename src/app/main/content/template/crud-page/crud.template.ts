import { Component, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TableCrud } from 'app/main/content/template/crud-table/crud.table.component';
import { TranslateService } from '@ngx-translate/core';

import { CrudService } from 'app/service/crud/crud.service';

import { fuseAnimations } from '@fuse/animations';

import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'crud-template',
    templateUrl: './crud.template.html',
    styleUrls  : ['./crud.template.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class CrudComponent<T> {

    @Input() title: string;
    @Input() iconTitle: string;
    @Input() service: CrudService<T>;
    @Input() table: TableCrud<T>;
    @Input() type: string;
    @Input() currentPage: string;
    @Input() form: FormGroup;

    @Output() onSave: EventEmitter<any> = new EventEmitter<any>();

    constructor(
        private translate: TranslateService,
        private router: Router
    ){
    }
    
    voltar(){
        this.router.navigate([this.currentPage]);
    }

    saveAction(event){
        this.onSave.emit({data: event});
    }

    isFormPage(){
        return this.type && this.type === 'form';
    }

    showRemoveButton(){
        if(this.table){
            return this.table.selected.length > 0 && this.table.component.loadingIndicator == false;
        }
        return false;
    }

    showEditButton(): boolean{
        if(this.table){
            return this.table.selected.length === 1 && this.table.component.loadingIndicator == false;
        }
        return false;
    }

    showNewButton() :boolean{
        if(this.table){
            return this.table.component.loadingIndicator == false;
        }
        return false;
    }

    newValue() {
        this.navigateToForm(null);
    }

    editValue(event: any) {
        this.navigateToForm(event.data.id);
    }

    inativeValue(event){
        event.preventDefault();
        this.table.loading = true;
        this.table.component.loadingIndicator = true;
        this.service.removeAll(this.table.selected).then(
            () => {
                this.table.component.loadingIndicator = false;
                this.table.loading = false;
                this.table.initDataTable();
            }
        );
    }

    navigateToForm(id){
        let url = this.currentPage + '/form';
        if(id){
            url+= '/' + id
        }
        this.router.navigate([url]);
    }
}
