import { Inject, ReflectiveInjector } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { AppModule } from 'app/app.module';


export abstract class CrudForm<T>{

    dialogTitle: string;
    modelForm: FormGroup;
    action: string;
    model: T;
    modelFormErrors: any;

    processing: boolean;
    protected formBuilder: FormBuilder = AppModule.injector.get(FormBuilder);

    constructor(@Inject(MAT_DIALOG_DATA) public data: any){
        this.action = data.action;
        if (this.action === 'edit') {
            this.dialogTitle = 'BRANCH.DIALOG.TITLE.EDIT';
            this.model = data.model;
        } else {
            this.dialogTitle = 'BRANCH.DIALOG.TITLE.NEW';
            this.model = this.getInstance();
        }
        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });
    }
    
    disableActions(): boolean{
        if(this.modelForm && this.modelForm.valid){
            return false;
        }
        return true;
    }

    onLoginFormValuesChanged(){
        for ( const field in this.modelFormErrors ){
            if ( !this.modelFormErrors.hasOwnProperty(field) ){
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if ( control && control.dirty && !control.valid ){
                this.modelFormErrors[field] = control.errors;
            }
        }
    }
    
    abstract createForm() : FormGroup;
    abstract getInstance() : T;

}