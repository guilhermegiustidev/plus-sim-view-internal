import { Component, ViewChild, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CrudService } from 'app/service/crud/crud.service';
import { Page } from '@fuse/components/page/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PostLoad } from 'app/rules/postload.component' 

declare var jquery: any;
declare var $: any;


@Component({
    selector: 'crud-table',
    templateUrl: './crud.table.component.html',
    styleUrls: ['./crud.table.component.scss'],

})
export class TableCrud<T>{

    @ViewChild(DatatableComponent) component: DatatableComponent;

    loading: boolean;
    page: Page<T>;
    selected: Array<T>;
    @Input() columns: any[];
    @Input() filter: T;
    @Input() service: CrudService<T>;
    @Output() rowClick: EventEmitter<any> = new EventEmitter();

    constructor() {
        this.page = new Page();
        this.page.number = 0;
        this.page.size = 100;
        this.page.numberOfElements = 0;
        this.selected = new Array<T>();
    }

    setPage(pageInfo) {
        this.page.number = pageInfo.offset;
        this.service.page(this.filter, this.page.size, pageInfo.offset, pageInfo.count)
            .subscribe(page => {
                this.page = page;
                this.page.numberOfElements = pageInfo.count;
                this.selected = new Array<T>();
            });

    }

    onActivate(event) {
    }


    ngAfterViewInit() {
        this.component.loadingIndicator = true;
        this.initDataTable();
    }

    public initDataTable() {
        this.service.count(this.filter).subscribe(
        count => {
            this.page.numberOfElements = count;
            this.setPage({ offset: 0, count: count });
            this.component.loadingIndicator = false;
        }, error =>{
            this.component.loadingIndicator = false;
            console.log(error);
        });
    }

}