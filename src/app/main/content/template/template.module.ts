import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';

import { CrudComponent } from './crud-page/crud.template';
import { TableCrud } from './crud-table/crud.table.component';

@NgModule({
    declarations: [
        CrudComponent,
        TableCrud
    ],
    exports: [
        CrudComponent,
        TableCrud
    ],
    imports     : [
        TranslateModule,
        FuseSharedModule
    ],

})

export class TemplateModule
{
}
