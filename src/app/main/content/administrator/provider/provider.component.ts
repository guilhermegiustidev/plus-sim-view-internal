import { Component, ViewEncapsulation } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { ProviderModel } from 'app/model/provider/provider.model';
import { ProviderBusiness } from 'app/service/provider/provider.service';


@Component({
    templateUrl: './provider.component.html',
    styleUrls: ['./provider.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class ProviderComponent {

    filter: ProviderModel;

    columns = [
        {prop: 'id', name: 'PROVIDER.TABLE.HEADER.ID'},
        {prop: 'name', name: 'PROVIDER.TABLE.HEADER.NAME'},
    ]

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private business: ProviderBusiness
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
    }

}
