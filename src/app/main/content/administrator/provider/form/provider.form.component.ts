import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { DatePipe } from '@angular/common';
import { ProviderBusiness } from 'app/service/provider/provider.service';
import { ProviderModel } from 'app/model/provider/provider.model';
import { ContactComponent } from '@fuse/components/contact/contact.component';

@Component({
    selector: 'provider-form',
    templateUrl: './provider.form.component.html',
    styleUrls: ['./provider.form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class ProviderFormComponent implements OnInit {

    id: number;
    modelForm: FormGroup;
    modelFormErrors: any;
    processing: boolean;
    name: string;
    @ViewChild(ContactComponent) contactComponent: ContactComponent;
    regex: string;

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private business: ProviderBusiness

    ) {

        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

        this.activeRoute.params.subscribe(params => {
            if (params['parameters']) {
                this.id = params['parameters'];
            }
        });

        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });

    }

    save(event) {
        let parameter: ProviderModel = {
            id : this.id,
            name: this.name,
            status: true,
            regex: this.regex,
            contacts : this.contactComponent.contacts
        }
        this.processing = true;
        this.business.merge(parameter).subscribe(
            success => {
                this.router.navigate(['/ADM/ADM10'])
            }, error => {
                this.processing = false;
            }
        );        
    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    ngOnInit(): void {
        this.modelFormErrors = {
            name: {}
        };
        if(this.id){
            this.business.findById(this.id).subscribe(
                success => {
                    this.name = success.name;
                    this.regex = success.regex;
                    this.contactComponent.contacts = success.contacts;
                }
            );
        }
    }


    createForm() {
        return this.formBuilder.group({
            name: [this.name, Validators.required],
            regex: [this.regex]
        });
    }

}
