export const locale = {
    lang: 'en',
    data: {
        'PROVIDER': {
            'HEADER': {
                'TITLE': 'Cadastro de Fornecedores'
            },
            'TABLE': {
                'HEADER': {
                    'ID': 'Id',
                    'NAME': 'Nome',
                    'REGEX': 'Expressão'
                }
            },
            'FORM': {
                'NAME': 'Nome',
                'REGEX': 'Expressão',
                'VALIDATOR': {
                    'NAME': {
                        'REQUIRED': 'Nome é obrigatório'
                    }
                }
            },
        }
    }
};
