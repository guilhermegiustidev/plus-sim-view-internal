export const locale = {
    lang: 'sp',
    data: {
        'BRANCH' : {
            'HEADER': {
                'TITLE' : 'Cadastro de Filiais'
            },            
            'TABLE': {
                'HEADER': {
                    'ID': 'Id',
                    'NAME': 'Nome',
                    'ADDRESS': 'Endereço',
                }
            }
        }
    }
};
