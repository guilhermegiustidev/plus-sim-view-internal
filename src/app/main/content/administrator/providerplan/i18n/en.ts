export const locale = {
    lang: 'en',
    data: {
        'PROVIDER_PLAN': {
            'HEADER': {
                'TITLE': 'Cadastro de Planos de Fornecedores'
            },
            'TABLE': {
                'HEADER': {
                    'ID': 'Id',
                    'COST': 'Custo',
                    'NAME': 'Nome'
                }
            },
            'FORM': {
                'COST': 'Preço',
                'PROVIDER': 'Fornecedor',
                'NAME': 'Nome',
                'VALIDATOR': {
                    'NAME': {
                        'REQUIRED': 'Nome do plano é obrigatório'
                    },
                    'PROVIDER': {
                        'REQUIRED' : 'Fornecedor é obrigatório'
                    },
                    'COST' : {
                        'REQUIRED': 'Preço é Obrigatório',
                        'INVALID': 'Valor de preço inválido'
                    }                  
                }
            },
        }
    }
};
