import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { ProviderPlanBusiness } from 'app/service/provider/provider.plan.service';
import { TypeServicePlanModel } from 'app/model/plan/type.service.plan.model';
import { ProviderModel } from 'app/model/provider/provider.model';
import { ProviderBusiness } from 'app/service/provider/provider.service';
import { AutocompleteTypeServiceComponent } from '@fuse/components/autocomplete/servicetype/autocomplete.service.type.component';

@Component({
    templateUrl: './provider.plan.form.component.html',
    styleUrls: ['./provider.plan.form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class ProviderPlanFormComponent implements OnInit {
    selectedProviderId: number;
    @ViewChild(AutocompleteTypeServiceComponent) autocomplete: AutocompleteTypeServiceComponent;

    id: number;
    modelForm: FormGroup;
    modelFormErrors: any;
    processing: boolean;

    status: boolean;

    days: number;

    cost: number;
    servicesType: Array<TypeServicePlanModel>;

    providers: Array<ProviderModel>;

    provider: ProviderModel;

    name: string;

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder, private service: ProviderPlanBusiness,
        private providerBusiness: ProviderBusiness
    ) {

        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

        this.activeRoute.params.subscribe(params => {
            if (params['parameters']) {
                this.id = params['parameters'];
            }
        });

        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });

    }

    findProvider(event){
        this.providerBusiness.findById(this.selectedProviderId).subscribe(
            success => {
                this.provider = success;
            }
        );
    }

    save(event) {
        this.processing = true;
        let parameter = 
        {
            id: this.id,
            name: this.name,
            status: this.status,
            cost: this.cost,
            provider: this.provider,
            services: this.autocomplete.selected
        }

        this.service.merge(parameter).subscribe(
            merged => {
                this.router.navigate(['/ADM/ADM09']);
            }, error => {
                this.processing = false;
            }
        );
    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    ngOnInit(): void {
        this.modelFormErrors = {
            cost: {},
            provider: {},
            name: {}
        };
        if (this.id) {
            this.service.findById(this.id).subscribe(
                success => {
                    this.cost = success.cost;
                    this.servicesType = success.services;
                    this.name = success.name;
                    if (success.provider) {
                        this.selectedProviderId = success.provider.id;
                        this.findProvider(null);
                    }

                }
            );
        }

        this.providerBusiness.findAll().subscribe(
            success => {
                this.providers = success
            }
        );

    }


    createForm() {
        return this.formBuilder.group({
            provider: [{ id: this.selectedProviderId }, Validators.required],
            name: [this.name, Validators.required],
            cost: [this.cost, [Validators.required, Validators.pattern("^[0-9]{1,3}([,.][0-9]{1,2})?$")]],
        });
    }

}
