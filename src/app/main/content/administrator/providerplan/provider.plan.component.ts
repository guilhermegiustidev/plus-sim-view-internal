import { Component, ViewEncapsulation } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { ProviderPlanModel } from 'app/model/provider/providerplan.model';
import { ProviderPlanBusiness } from 'app/service/provider/provider.plan.service';


@Component({
    templateUrl: './provider.plan.component.html',
    styleUrls: ['./provider.plan.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class ProviderPlanComponent {

    filter: ProviderPlanModel;

    columns = [
        {prop: 'id', name: 'PROVIDER_PLAN.TABLE.HEADER.ID'},
        {prop: 'name', name: 'PROVIDER_PLAN.TABLE.HEADER.NAME'},        
        {prop: 'cost', name: 'PROVIDER_PLAN.TABLE.HEADER.COST'},
    ]

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private business: ProviderPlanBusiness
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
    }

}
