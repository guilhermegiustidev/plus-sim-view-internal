import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';

import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSidenavModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
import { LoggedGuard } from '@fuse/guard/looged.guard';

import { TemplateModule } from 'app/main/content/template/template.module';
import { ServiceModule } from 'app/service/service.module';
import { ProviderPlanComponent } from './provider.plan.component';
import { ProviderPlanFormComponent } from './form/provider.plan.form.component';



const routes: Routes = [
    {
        path: 'ADM09',
        component: ProviderPlanComponent,
    },
    {
        path: 'ADM09/form/:parameters',
        component: ProviderPlanFormComponent,
    },
    {
        path: 'ADM09/form',
        component: ProviderPlanFormComponent,
    }


];

@NgModule({
    declarations: [
        ProviderPlanComponent,
        ProviderPlanFormComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CdkTableModule,
        TemplateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSidenavModule,
        MatTableModule,
        MatToolbarModule,
        FuseSharedModule,
        FuseConfirmDialogModule,
        ServiceModule,
    ]
})
export class ProviderPlanModule {
}
