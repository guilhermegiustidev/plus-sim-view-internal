import { Component, ViewEncapsulation } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { PartnerModel } from 'app/model/partner/partner.model';
import { PartnerBusiness } from 'app/service/partner/partner.service';

@Component({
    templateUrl: './partner.component.html',
    styleUrls: ['./partner.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class PartnerComponent {

    filter: PartnerModel;

    columns = [
        {prop: 'id', name: 'PARTNER.TABLE.HEADER.ID'},
        {prop: 'name', name: 'PARTNER.TABLE.HEADER.NAME'},
    ]

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private partnerBusiness: PartnerBusiness
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
    }

}
