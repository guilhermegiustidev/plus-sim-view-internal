export const locale = {
    lang: 'en',
    data: {
        'PARTNER': {
            'HEADER': {
                'TITLE': 'Cadastro de Representantes'
            },
            'TABLE': {
                'HEADER': {
                    'ID': 'Id',
                    'NAME': 'Nome',
                }
            },
            'FORM': {
                'ADDRESS': 'Endereços',
                'CONTACT': 'Contatos',
                'NAME': 'Name',
                'USERS': 'Users',
                'VALIDATOR': {
                    'NAME': {
                        'REQUIRED': 'Nome da filial obrigatório'
                    },
                }
            },
        }
    }
};
