export const locale = {
    lang: 'en',
    data: {
        'BRANCH': {
            'HEADER': {
                'TITLE': 'Cadastro de Filiais'
            },
            'TABLE': {
                'HEADER': {
                    'ID': 'Id',
                    'NAME': 'Nome',
                    'ADDRESS': 'Endereço',
                }
            },
            'FORM': {
                'USERS': 'Usuários',
                'NAME': 'Nome da Filial',
                'COUNTRY': 'País',
                'STATE': 'Estado',
                'CITY': 'Cidade',
                'ADDRESS': 'Endereço',
                'ADD_ADDRESS': 'Adicionar Endereço',
                'ADD_CONTACT': 'Adicionar Contato',
                'PHONE': 'Telefone',
                'ZIP': 'Cep',
                'CONTACT': 'Contato',
                'LOCALES': 'Idiomas',
                'TYPE': {
                    'EMAIL': 'Email',
                    'PHONE': 'Telefone'
                },
                'VALIDATOR': {
                    'NAME': {
                        'REQUIRED': 'Nome da filial obrigatório'
                    },
                    'ADDRESS': {
                        'REQUIRED': 'Endereço é obrigatório'
                    },
                    'PHONE': {
                        'REQUIRED': 'Telefone é obrigatório',
                        'INVALID': 'Telefone inválido',
                    },
                    'COUNTRY': {
                        'REQUIRED': 'País é obrigatório',
                    },
                    'STATE': {
                        'REQUIRED': 'Estado é obrigatório',
                    },
                    'CITY': {
                        'REQUIRED': 'Cidade é obrigatória',
                    },
                    'ZIP': {
                        'REQUIRED': 'Cep é obrigatório',
                        'INVALID': 'Cep inválido',
                    }
                }
            },
        }
    }
};
