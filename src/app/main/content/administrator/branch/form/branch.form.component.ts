import { Component, ViewEncapsulation, OnInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BranchModel } from 'app/model/branch/branch.model';
import { CountryModel } from 'app/model/country.model';
import { AddressModel } from 'app/model/address.model';
import { StateModel } from 'app/model/state.model';
import { CityModel } from 'app/model/city.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { CityBusiness } from 'app/service/location/city.service';
import { StateBusiness } from 'app/service/location/state.service';
import { CountryBusiness } from 'app/service/location/country.service';

import { fuseAnimations } from '@fuse/animations';

import { MatDialog } from '@angular/material';

import { AddressDialogComponent } from '@fuse/components/address/dialog/address.component';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { BranchAddress } from 'app/model/branch/branch.address.model';
import { BranchBusiness } from '../../../../../service/branch/branch.service';
import { BranchContact } from '../../../../../model/branch/branch.contact.model';
import { ContactModel } from 'app/model/contact.model';
import { AutocompleteUserComponent } from '@fuse/components/autocomplete/users/autocomplete.user.component';
import { AddressComponent } from '@fuse/components/address/address.component';
import { ContactComponent } from '@fuse/components/contact/contact.component';

@Component({
    selector: 'branch-form',
    templateUrl: './branch.form.component.html',
    styleUrls: ['./branch.form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class BranchFormDialog implements OnInit {

    @ViewChild(AutocompleteUserComponent) userAutocomplete: AutocompleteUserComponent;
    @ViewChild(AddressComponent) addressComponent: AddressComponent;
    @ViewChild(ContactComponent) contactComponent: ContactComponent;

    id: number;
    modelForm: FormGroup;
    modelFormErrors: any;
    processing: boolean;
    name: string;
    dialogRef: any;

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private dialog: MatDialog,
        private branchBusiness: BranchBusiness
    ) {

        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

        this.activeRoute.params.subscribe(params => {
            if (params['parameters']) {
                this.id = params['parameters'];
            }
        });

        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });

    }

    save(event) {
        let parameter = {
            branchId: this.id,
            branchName: this.name,
            users: this.userAutocomplete.selected,
            addresses: this.addressComponent.addressess,
            contacts: this.contactComponent.contacts
        }
        this.processing = true;
        this.branchBusiness.merge(parameter).subscribe(
            success => {
                this.router.navigate(['/ADM/ADM01'])
            }, error => {
                this.processing = false;
            }
        );
    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    ngOnInit(): void {
        this.modelFormErrors = {
            name: {},
        };
        if (this.id) {
            this.branchBusiness.findById(this.id).subscribe(
                success => {
                    this.name = success.name
                });
            this.branchBusiness.findAddressesBranch(this.id).subscribe(
                success => {
                    this.addressComponent.addressess = success
                });
            this.branchBusiness.findContactsBranch(this.id).subscribe(
                success => {
                    this.contactComponent.contacts = success
                });
            this.branchBusiness.findUsersBranch(this.id).subscribe(
                success => {
                    this.userAutocomplete.selected = success
                });
        }
    }


    createForm() {
        return this.formBuilder.group({
            id: [this.id],
            name: [this.name, Validators.required],
        });
    }

    getInstance(): BranchModel {
        let branch = new BranchModel();
        return branch;
    }
}
