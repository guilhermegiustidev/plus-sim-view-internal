import { Component, ViewEncapsulation } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

import { BranchModel } from 'app/model/branch/branch.model';
import { BranchFormDialog } from './form/branch.form.component';
import { BranchBusiness } from 'app/service/branch/branch.service';

@Component({
    templateUrl: './branch.component.html',
    styleUrls: ['./branch.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class BranchComponent {

    filter: BranchModel;

    columns = [
        {prop: 'id', name: 'BRANCH.TABLE.HEADER.ID'},
        {prop: 'name', name: 'BRANCH.TABLE.HEADER.NAME'},
    ]

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private branchBusiness: BranchBusiness
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
    }


}
