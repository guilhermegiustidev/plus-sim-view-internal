export const locale = {
    lang: 'en',
    data: {
        'USER' :{
            'HEADER' : {
                'TITLE' : 'Cadastro de Usuário'
            },
            'TABLE' : {
                'HEADER' : {
                    'ID' : 'Id',
                    'NAME' : 'Nome'
                }
            },
            'FORM' : {
                'NAME' : 'Nome',
                'LOGIN': 'Login',
                'PARTNER' : 'Representantes',
                'BRANCHS' : 'Filiais',
                'PROFILES': 'Perfis',
                'CONTACT' : 'Contatos',
                'CHECK_LOGIN' : 'Verificar Login',
                'VALIDATOR': {
                    'NAME':{
                        'REQUIRED': 'Nome é obrigatório'
                    },                    
                    'LOGIN':{
                        'REQUIRED': 'Login é obrigatório',
                        'INVALID': 'Login Inváligo',
                        'EXISTS': 'Login já cadastrado'
                    },
                }
            }
        }
    }
};
