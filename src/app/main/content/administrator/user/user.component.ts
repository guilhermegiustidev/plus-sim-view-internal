import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';

import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { ProfileBusiness } from 'app/service/profile/profile.service';
import { ProfileModel } from 'app/model/profile/profile.model';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { UserModel } from 'app/model/user.model';
import { UserBusiness } from 'app/service/user/user.service';

@Component({
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class UserComponent {

    filter: UserModel;

    columns = [
        {prop: 'id', name: 'USER.TABLE.HEADER.ID'},
        {prop: 'name', name: 'USER.TABLE.HEADER.NAME'},
    ]

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private userBusiness: UserBusiness
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
    }

}
