import { Component, ViewEncapsulation, OnInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { MatDialog } from '@angular/material';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { ProfileModel } from 'app/model/profile/profile.model';
import { ProfileBusiness } from 'app/service/profile/profile.service';
import { RoleProfile } from 'app/model/profile/role.profile.model';
import { UserProfile } from 'app/model/profile/user.profile.model';
import { UserBusiness } from 'app/service/user/user.service';
import { UserModel } from 'app/model/user.model';
import { ModuleProfileModel } from '../../../../../model/profile/module.profile.model';
import { AutocompleteUserComponent } from '@fuse/components/autocomplete/users/autocomplete.user.component';
import { AddressComponent } from '@fuse/components/address/address.component';
import { ContactComponent } from '@fuse/components/contact/contact.component';
import { BranchAddress } from 'app/model/branch/branch.address.model';
import { BranchModel } from 'app/model/branch/branch.model';
import { PartnerModel } from 'app/model/partner/partner.model';
import { ProfileComponent } from 'app/main/content/administrator/profile/profile.component';
import { AutocompleteProfileComponent } from '@fuse/components/autocomplete/profile/autocomplete.profile.component';


@Component({
    templateUrl: './user.form.component.html',
    styleUrls: ['./user.form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class UserFormComponent implements OnInit {

    @ViewChild(AutocompleteProfileComponent) profileComponent: AutocompleteProfileComponent;
    @ViewChild(ContactComponent) contactComponent: ContactComponent;

    id: number;
    modelForm: FormGroup;
    modelFormErrors: any;
    processing: boolean;
    dialogRef: any;
    branchs: Array<BranchModel>;
    partners: Array<PartnerModel>;
    name: string;
    login: string;

    exists: boolean;

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private userBusiness: UserBusiness,
    ) {

        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

        this.activeRoute.params.subscribe(params => {
            if (params['parameters']) {
                this.id = params['parameters'];
            }
        });

        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });
        this.modelFormErrors = {
            name: {},
            login: {}
        };
    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    ngOnInit(): void {
        if (this.id) {
            this.userBusiness.findById(this.id).subscribe(
                success => {
                    this.name = success.name;
                    this.login = success.login;
                }
            );
        }
        this.userBusiness.findBranchsUser(this.id).subscribe(
            success => {
                this.branchs = success;
            }
        );
        this.userBusiness.findPartnersUser(this.id).subscribe(
            success => {
                this.partners = success;
            }
        );
        if (this.id) {
            this.userBusiness.findProfilesUser(this.id).subscribe(
                success => {
                    this.profileComponent.selected = success;
                }
            );
            this.userBusiness.findContactUser(this.id).subscribe(
                success => {
                    this.contactComponent.contacts = success;
                }
            );
        }

    }


    createForm() {
        return this.formBuilder.group({
            name: [this.name, Validators.required],
            login: [{ value: this.login, disabled: this.id }, [Validators.required]]
        });
    }

    save(event) {
        let parameters = {
            personId: this.id,
            personName: this.name,
            personLogin: this.login,
            contacts: this.contactComponent.contacts,
            profiles: this.profileComponent.selected,
            branchs: this.branchs,
            partners: this.partners
        }

        this.processing = true;
        this.userBusiness.merge(parameters).subscribe(
            success => {
                this.router.navigate(['/ADM/ADM02'])
            }, error => {
                this.processing = false;
            }
        );
    }

    disabledLogin() {
        return this.id !== null;
    }

    verifyLogin(event) {
        let value = event.target.value;
        if (value) {
            this.userBusiness.verifyLogin(value).subscribe(
                success => {
                    if (success) {
                        this.modelForm.controls['login'].setErrors({ exists: success });
                        this.modelFormErrors['login'] = this.modelForm.controls['login'].errors;
                    }
                }
            );
        }
    }

}
