export const locale = {
    lang: 'en',
    data: {
        'PROFILE' :{
            'HEADER' : {
                'TITLE' : 'Cadastro de Perfil'
            },
            'TABLE' : {
                'HEADER' : {
                    'ID' : 'Id',
                    'NAME' : 'Nome'
                }
            },
            'FORM' : {
                'ROLES': {
                    'TITLE' : 'Permissões do sistema'
                },
                'MENU': {
                    'TITLE' : 'Menu do sistema'
                },
                'USERS': {
                    'TITLE' : 'Usuários'
                },
                'SEARCH_USERS': {
                    'TITLE' : 'Procurar Usuários'
                },
                'SELECTED_USERS': {
                    'TITLE' : 'Usuários Selecionados'
                },
                'NAME' : 'Nome',
                'VALIDATOR': {
                    'NAME':{
                        'REQUIRED': 'Nome é obrigatório'
                    },
                }
            }
        }
    }
};
