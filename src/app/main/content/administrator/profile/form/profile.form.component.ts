import { Component, ViewEncapsulation, OnInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { MatDialog } from '@angular/material';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { ProfileModel } from 'app/model/profile/profile.model';
import { ProfileBusiness } from 'app/service/profile/profile.service';
import { RoleProfile } from 'app/model/profile/role.profile.model';
import { UserProfile } from 'app/model/profile/user.profile.model';
import { UserBusiness } from 'app/service/user/user.service';
import { UserModel } from 'app/model/user.model';
import { ModuleProfileModel } from '../../../../../model/profile/module.profile.model';
import { AutocompleteUserComponent } from '@fuse/components/autocomplete/users/autocomplete.user.component';


@Component({
    templateUrl: './profile.form.component.html',
    styleUrls: ['./profile.form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class ProfileForm implements OnInit {

    @ViewChild(AutocompleteUserComponent) userAutocomplete: AutocompleteUserComponent;

    idProfile       : number;
    modelForm       : FormGroup;
    modelFormErrors : any;
    processing      : boolean;
    dialogRef       : any;
    model           : ProfileModel;
    roles           : Array<RoleProfile>;
    modules         : Array<ModuleProfileModel>;

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private profileBusiness : ProfileBusiness,
        private userBusiness: UserBusiness,
    ) {

        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

        this.model = new ProfileModel();
        this.activeRoute.params.subscribe(params => {
            if (params['parameters']) {
                this.idProfile = params['parameters'];
                if(this.idProfile){
                    this.profileBusiness.findById(this.idProfile).subscribe(
                        success => {
                            this.model = success;
                        }
                    );
                }
            }
        });
        
        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });
        this.modelFormErrors = {
            description: {},
        };
    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    ngOnInit(): void {
        this.profileBusiness.findRolesProfile(this.idProfile).subscribe(
            success => {
                this.roles = success;
            }
        );

        this.profileBusiness.findAllUsers(this.idProfile).subscribe(
            success => {
                if(success){
                    this.userAutocomplete.selected = new Array<UserModel>();
                    for(let index of success){
                        let user: UserModel = new UserModel();
                        user.id = index.idUser;
                        user.name = index.userName;
                        user.status = index.status;
                        this.userAutocomplete.selected.push(user);
                    }
                }
            }
        ); 
        this.profileBusiness.findAllPrograms(this.idProfile).subscribe(
            success => {
                this.modules = success;
            }
        );
    }


    createForm() {
        return this.formBuilder.group({
            description: [this.model.description, Validators.required],
        });
    }

    save(event){

        let usersProfile = new Array<UserProfile>();
        if(this.userAutocomplete && this.userAutocomplete.selected){
            for(let index of this.userAutocomplete.selected){
                let userProfile = new UserProfile();
                userProfile.idProfile = this.idProfile;
                userProfile.idUser = index.id;
                userProfile.status = index.status;
                userProfile.userName = index.name;
                usersProfile.push(userProfile);
            }
        }

        event.data.preventDefault();
        if(this.modelForm.invalid == false){
            this.processing = true;
            this.profileBusiness.merge({
                idProfile   : this.idProfile,
                nameProfile : this.model.description,
                users    : usersProfile,
                roles    : this.roles,
                modules   : this.modules,
                status   : true
            }).subscribe(
                success => {
                    this.router.navigate(['/ADM/ADM06'])
                }, error => {
                    this.processing = false;
                }
            );
        }
    }

}
