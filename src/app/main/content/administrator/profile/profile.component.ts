import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';

import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { ProfileBusiness } from 'app/service/profile/profile.service';
import { ProfileModel } from 'app/model/profile/profile.model';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';

@Component({
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class ProfileComponent {

    filter: ProfileModel;

    columns = [
        {prop: 'id', name: 'PROFILE.TABLE.HEADER.ID'},
        {prop: 'description', name: 'PROFILE.TABLE.HEADER.NAME'},
    ]

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private profileBusiness: ProfileBusiness
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
    }

}
