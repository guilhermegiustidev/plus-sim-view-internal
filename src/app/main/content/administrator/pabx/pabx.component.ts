import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';

import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { ProfileBusiness } from 'app/service/profile/profile.service';
import { ProfileModel } from 'app/model/profile/profile.model';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { PabxBusiness } from 'app/service/pabx/pabx.service';
import { PabxModel } from 'app/model/pabx/pabx.model';

@Component({
    templateUrl: './pabx.component.html',
    styleUrls: ['./pabx.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class PabxComponent {

    filter: PabxModel;

    columns = [
        {prop: 'id', name: 'PABX.TABLE.HEADER.ID'},
        {prop: 'numberLine', name: 'PABX.TABLE.HEADER.NUMBER'},
        {prop: 'city.name', name: 'PABX.TABLE.HEADER.CITY_NAME'},
        {prop: 'country.name', name: 'PABX.TABLE.HEADER.COUNTRY_NAME'},
        {prop: 'statusEnum', name: 'PABX.TABLE.HEADER.STATUS_LINE'}
        
    ]

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private business: PabxBusiness
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
    }

}
