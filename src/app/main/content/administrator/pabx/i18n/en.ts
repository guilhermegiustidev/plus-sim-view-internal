export const locale = {
    lang: 'en',
    data: {
        'PABX' :{
            'HEADER' : {
                'TITLE' : 'Cadastro de Pabx'
            },
            'TABLE' : {
                'HEADER' : {
                    'ID' : 'Id',
                    'NUMBER' : 'Número',
                    'CITY_NAME' : 'Cidade',
                    'COUNTRY_NAME' : 'País',
                    'STATUS_LINE' : 'Status da linha'
                }
            },
            'FORM' : {
                'NUMBER' : 'Número',
                'COUNTRY': 'País',
                'STATE' : 'Estado',
                'CITY_NAME': 'Nome da Cidade',
                'STATE_NAME': 'Nome do Estado',
                'STATE_ALL' : 'Todos',
                'VALIDATOR': {
                    'COUNTRY': {
                        'REQUIRED' : 'País é obrigatório'
                    },
                    'NUMBER': {
                        'REQUIRED': 'Numero da Linha Pabx é obrigatório',
                        'PATTERN': 'Padrão de telefone inválido'
                    }

                }
            }
        }
    }
};
