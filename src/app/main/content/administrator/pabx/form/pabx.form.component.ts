import { Component, ViewEncapsulation, OnInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { MatDialog } from '@angular/material';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { CountryModel } from 'app/model/country.model';
import { CityModel } from 'app/model/city.model';
import { StateModel } from 'app/model/state.model';
import { CountryBusiness } from 'app/service/location/country.service';
import { CityBusiness } from 'app/service/location/city.service';
import { StateBusiness } from 'app/service/location/state.service';
import { Page } from '@fuse/components/page/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PabxBusiness } from 'app/service/pabx/pabx.service';
import { PabxModel } from 'app/model/pabx/pabx.model';
import { CityComponent } from '@fuse/components/city/city.component';


@Component({
    templateUrl: './pabx.form.component.html',
    styleUrls: ['./pabx.form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class PabxForm implements OnInit {
    
    @ViewChild(CityComponent) cityComponent: CityComponent;
    
    idPabx: number;
    modelForm: FormGroup;
    modelFormErrors: any;
    processing: boolean;
    
    numberLine: string;
    status: boolean;
    
    columns = [
        { prop: 'name', name: 'name' },
        { prop: 'state.name', name: 'state.name' },
    ]
    
    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private business: PabxBusiness
        
    ) {
        
        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);
        this.activeRoute.params.subscribe(params => {
            if (params['parameters']) {
                this.idPabx = params['parameters'];
            }
        });
        
        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });
        this.modelFormErrors = {
            numberLine: {},
            country: {},
            city: {}
        };
    }
    
    ngOnInit(): void {
        if (this.idPabx) {
            this.business.findById(this.idPabx).subscribe(
                success => {
                    this.numberLine = success.numberLine;
                    this.status = success.status;
                    this.cityComponent.city = success.city;
                }
            );
        } else {
            this.status = true;
        }
    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }
            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }
    onSelect(event) {

    }

    createForm() {
        return this.formBuilder.group({
            numberLine: [this.numberLine, [Validators.required, Validators.pattern("^[0-9]*$")]],
        });
    }

    save(event) {
        if (this.cityComponent.city) {
            let parameter: PabxModel = {
                id: this.idPabx,
                numberLine: this.numberLine,
                status: true,
                country: this.cityComponent.city.state.country,
                city: this.cityComponent.city,
                statusEnum: null
            }
            this.processing = true;
            this.business.merge(parameter).subscribe(
                success => {
                    this.router.navigate(['/ADM/ADM07'])
                }, error => {
                    this.processing = false;
                }
            );
        }
    }

}
