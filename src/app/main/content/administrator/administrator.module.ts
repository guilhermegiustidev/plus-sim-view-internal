import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import { TranslateModule } from '@ngx-translate/core';

import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSidenavModule, MatTableModule, MatToolbarModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { LoggedGuard } from '@fuse/guard/looged.guard';

import { BranchModule } from 'app/main/content/administrator/branch/branch.module';
import { ProfileModule } from 'app/main/content/administrator/profile/profile.module';
import { PartnerModule } from 'app/main/content/administrator/partner/partner.module';
import { UserModule } from 'app/main/content/administrator/user/user.module';
import { PlanModule } from 'app/main/content/administrator/plans/plan.module';
import { LineModule } from 'app/main/content/administrator/line/line.module';
import { PabxModule } from 'app/main/content/administrator/pabx/pabx.module';
import { ProviderModule } from 'app/main/content/administrator/provider/provider.module';
import { ProviderPlanModule } from 'app/main/content/administrator/providerplan/provider.plan.module';
import { TypeServicePlanModule } from 'app/main/content/administrator/planservicetype/type.service.plan.module';
import { SinModule } from 'app/main/content/administrator/sin/sin.module';

@NgModule({

    imports: [
        TranslateModule,
        FuseSharedModule,
        CdkTableModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSidenavModule,
        MatTableModule,
        MatToolbarModule,
        FuseSharedModule,
        FuseConfirmDialogModule,
        BranchModule,
        ProfileModule,
        PartnerModule,
        UserModule,
        PlanModule,
        LineModule,
        PabxModule,
        ProviderModule,
        ProviderPlanModule,
        TypeServicePlanModule,
        SinModule
    ],

})

export class AdministratorModule {
}
