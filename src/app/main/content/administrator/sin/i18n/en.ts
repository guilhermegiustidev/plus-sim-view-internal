export const locale = {
    lang: 'en',
    data: {
        'LOT': {
            'CONFIRM': {
                'EXISTS': {
                    'TITLE': 'Continuar cadastro de Chips',
                    'ERROR.BODY': 'Existe um cadastro de chips em andamento, deseja continuar?'
                }
            },

            'HEADER': {
                'TITLE': 'Cadastro de Chips'
            },
            'TABLE': {
                'HEADER': {
                    'ID': 'Id',
                }
            },
            'FORM': {
                'CODE_SIM': 'Código do Chip',
                'CODE': 'Código do Lot',
                'PROVIDER': 'Operadora',
                'VALIDATOR': {
                    'SIN_CODE': {
                        'PATTERN': 'Padão de codigo de Barras inválido'
                    }
                }
            },
        }
    }
};
