import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';

import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSidenavModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
import { LoggedGuard } from '@fuse/guard/looged.guard';

import { TemplateModule } from 'app/main/content/template/template.module';
import { ServiceModule } from 'app/service/service.module';
import { SinFormComponent } from 'app/main/content/administrator/sin/form/sin.form.component';
import { SinComponent } from 'app/main/content/administrator/sin/sin.component';

const routes: Routes = [
    {
        path: 'ADM12',
        component: SinComponent,
    },
    {
        path: 'ADM12/form/:parameters',
        component: SinFormComponent,
    },
    {
        path: 'ADM12/form',
        component: SinFormComponent,
    }


];

@NgModule({
    declarations: [
        SinComponent,
        SinFormComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CdkTableModule,
        TemplateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSidenavModule,
        MatTableModule,
        MatToolbarModule,
        FuseSharedModule,
        FuseConfirmDialogModule,
        ServiceModule,
    ]
})
export class SinModule {
}
