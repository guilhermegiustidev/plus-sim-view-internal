import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { SinService } from 'app/service/sin/sin.service';
import { LotService } from 'app/service/sin/lot.service';
import { ProviderBusiness } from 'app/service/provider/provider.service';
import { ProviderPlanFormComponent } from 'app/main/content/administrator/providerplan/form/provider.plan.form.component';
import { ProviderModel } from 'app/model/provider/provider.model';
import { SinModel } from 'app/model/sin/sin.model';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { TranslateService } from '@ngx-translate/core';
import { LotModel } from 'app/model/sin/lot.model';
import { Page } from '@fuse/components/page/page';

declare var swal: any;

@Component({
    templateUrl: './sin.form.component.html',
    styleUrls: ['./sin.form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class SinFormComponent implements OnInit {
    @ViewChild('tableSim') table: DatatableComponent;

    id: number;
    modelForm: FormGroup;
    modelFormErrors: any;
    processing: boolean;
    code: string;
    providers: Array<ProviderModel>;

    provider: ProviderModel;

    selectedProvider: number;

    sins: Array<SinModel> = new Array<SinModel>();
    sinCode: string;
    status: string;

    page: Page<SinModel>;
    selected: Array<SinModel>;
    columns: any[];
    filter: SinModel;

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private sinService: SinService,
        private lotService: LotService,
        private providerService: ProviderBusiness,
        private translate: TranslateService

    ) {
        this.page = new Page();
        this.page.number = 0;
        this.page.size = 100;
        this.page.numberOfElements = 0;
        this.selected = new Array<SinModel>();

        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

        this.activeRoute.params.subscribe(params => {
            if (params['parameters']) {
                this.id = params['parameters'];
            }
        });

        this.providerService.findAll().subscribe(
            success => {
                this.providers = success;
            }
        );

        this.buildForm();

    }

    private buildForm() {
        this.modelForm = this.createForm();
        this.onValuesChangeForm();
    }

    private onValuesChangeForm() {
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });
    }

    selectProvider(event) {
        if (this.selectedProvider) {
            this.providerService.findById(this.selectedProvider).subscribe(
                success => {
                    let regex = success.regex;
                    this.provider = success;
                    this.modelForm.controls.sinCode.validator = this.regexValidator();
                }
            );
        }
    }

    save(event) {
    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    ngOnInit(): void {
        this.modelFormErrors = {
            code: {},
            provider: {},
            sinCode: {}
        };

        if (!this.id) {
            this.lotService.verifyProgress().subscribe(
                success => {
                    if (success) {
                        return new Promise((resolve, reject) => {
                            Promise.all([
                                this.translate.get('LOT.CONFIRM.EXISTS.TITLE'),
                                this.translate.get('LOT.CONFIRM.EXISTS.ERROR.BODY'),
                                this.translate.get('CONFIRM.EXCLUDE.YES'),
                                this.translate.get('CONFIRM.EXCLUDE.NO'),
                            ]).then(
                                (success) => {
                                    swal(success[0]['value'], success[1]['value'], {
                                        buttons: {
                                            no: {
                                                text: success[3]['value'],
                                                value: "no",
                                            },
                                            yes: {
                                                text: success[2]['value'],
                                                value: "yes",
                                            },
                                        },
                                        showLoaderOnConfirm: true
                                    }).then((value) => {
                                        switch (value) {
                                            case "yes":
                                                this.generateLot('INIT');
                                                break;
                                            case "no":
                                                this.generateLot('PROGRESS');
                                                break;
                                            default:
                                                resolve();
                                                break;
                                        }
                                    });
                                },
                                reject
                                );
                        });
                    } else {
                        this.generateLot('INIT');
                    }
                },
                error => {
                    this.generateLot('INIT');
                }
            );
        } else {
            this.lotService.findById(this.id).subscribe(
                success => {
                    this.code = success.code;
                    this.sins = success.sins;
                    this.status = success.status;
                    if (!this.sins) {
                        this.sins = new Array<SinModel>();
                    }
                },
                error => {
                    this.redirectToList();
                }
            );
        }
    }

    private generateLot(status: string) {
        this.processing = true;
        this.lotService.generateLot(status).subscribe(
            success => {
                this.processing = false;
                this.buildLot(success);
            }, error => {
                this.redirectToList();
            }
        );
    }

    buildLot(lotModel: LotModel) {
        this.id = lotModel.id;
        this.code = lotModel.code;
        this.status = lotModel.status;
        this.sins = lotModel.sins;
        if (!this.sins) {
            this.sins = new Array<SinModel>();
        }
    }

    private redirectToList() {
        this.router.navigate(['/ADM/ADM12']);
    }

    onChangeSinCode(event) {
        if (this.provider) {
            let value = event.target.value;
            /*
            if (value) {
                this.table.loadingIndicator = true;
            } else {
                this.table.loadingIndicator = false;
            }
            */
            let regex = new RegExp(this.provider.regex);
            let regexValue = regex.exec(value);
            if (regexValue) {
                this.sinService.addSin(this.provider.id, this.sinCode, this.id).subscribe(
                    success => {
                        this.sinCode = null;
                    },
                );
            }
        }
    }

    regexValidator() {
        return Validators.pattern(this.provider.regex);
    }

    createForm() {
        return this.formBuilder.group({
            code: [{ value: this.code, disabled: true }, [Validators.required]],
            provider: [this.selectedProvider, Validators.required],
            sinCode: [this.sinCode]
        });
    }

    setPage(pageInfo) {
        this.page.number = pageInfo.offset;
        this.sinService.page(this.filter, this.page.size, pageInfo.offset, pageInfo.count)
            .subscribe(page => {
                this.page = page;
                this.page.numberOfElements = pageInfo.count;
                this.selected = new Array<SinModel>();
            });

    }

    public initDataTable() {
        this.sinService.count(this.filter).subscribe(
            count => {
                this.page.numberOfElements = count;
                this.setPage({ offset: 0, count: count });
                this.table.loadingIndicator = false;
            }, error => {
                this.table.loadingIndicator = false;
            });
    }

    ngAfterViewInit() {
        this.table.loadingIndicator = true;
        this.initDataTable();
    }

    onActivate(event) {
    }

}
