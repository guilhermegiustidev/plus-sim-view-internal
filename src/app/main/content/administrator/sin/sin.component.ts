import { Component, ViewEncapsulation } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { SinModel } from 'app/model/sin/sin.model';
import { SinService } from 'app/service/sin/sin.service';
import { LotService } from 'app/service/sin/lot.service';


@Component({
    templateUrl: './sin.component.html',
    styleUrls: ['./sin.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class SinComponent {

    filter: SinModel;

    columns = [
        {prop: 'id', name: 'LOT.TABLE.HEADER.ID'},
        {prop: 'status', name: 'LOT.TABLE.HEADER.STATUS'},
        {prop: 'sims.length', name: 'LOT.TABLE.HEADER.SIMS'},
    ]

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private service: LotService
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
    }

}
