import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';

import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSidenavModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
import { LoggedGuard } from '@fuse/guard/looged.guard';

import { PlanComponent } from './plan.component';
import { PlanFormComponent } from './form/plan.form.component';
import { TemplateModule } from 'app/main/content/template/template.module';
import { ServiceModule } from 'app/service/service.module';

import { AddressModule } from '@fuse/components/address/address.module';


const routes: Routes = [
    {
        path: 'ADM05',
        component: PlanComponent,
    },
    {
        path: 'ADM05/form/:parameters',
        component: PlanFormComponent,
    },
    {
        path: 'ADM05/form',
        component: PlanFormComponent,
    }


];

@NgModule({
    declarations: [
        PlanComponent,
        PlanFormComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        CdkTableModule,
        TemplateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSidenavModule,
        MatTableModule,
        MatToolbarModule,
        FuseSharedModule,
        FuseConfirmDialogModule,
        ServiceModule,
    ]
})
export class PlanModule {
}
