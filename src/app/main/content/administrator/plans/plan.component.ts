import { Component, ViewEncapsulation } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { PlanBusiness } from 'app/service/plan/plan.service';
import { PlanModel } from 'app/model/plan/plan.model';


@Component({
    templateUrl: './plan.component.html',
    styleUrls: ['./plan.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class PlanComponent {

    filter: PlanModel;

    columns = [
        {prop: 'id', name: 'PLAN.TABLE.HEADER.ID'},
        {prop: 'cost', name: 'PLAN.TABLE.HEADER.COST'},
        {prop: 'price', name: 'PLAN.TABLE.HEADER.PRICE'},
        {prop: 'days', name: 'PLAN.TABLE.HEADER.DAYS'},
    ]

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private planBusiness: PlanBusiness
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
    }

}
