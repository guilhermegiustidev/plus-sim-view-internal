export const locale = {
    lang: 'en',
    data: {
        'PLAN': {
            'HEADER': {
                'TITLE': 'Cadastro de Planos'
            },
            'TABLE': {
                'HEADER': {
                    'ID': 'Id',
                    'COST': 'Custo',
                    'PRICE': 'Preço',
                    'DAYS': 'Dias',
                    'MINUTES': 'Minutos'
                }
            },
            'FORM': {
                'PRICE': 'Preço',
                'COST': 'Custo',
                'DAYS': 'Dias',
                'VALIDATOR': {
                    'PRICE': {
                        'REQUIRED': 'Preço é obrigatório',
                        'INVALID': 'Preço Inválido'
                    },
                    'COST': {
                        'REQUIRED': 'Custo é obrigatório',
                        'INVALID': 'Custo Inválido'
                    },
                    'DAYS': {
                        'REQUIRED': 'Dias é obrigatório',
                        'INVALID': 'Valor de dias inválido'
                    },
                    
                }
            },
        }
    }
};
