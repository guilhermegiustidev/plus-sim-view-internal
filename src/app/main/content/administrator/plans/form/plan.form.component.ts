import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { PlanBusiness } from 'app/service/plan/plan.service';
import { PlanModel } from 'app/model/plan/plan.model';
import { TypeServicePlanModel } from 'app/model/plan/type.service.plan.model';
import { AutocompleteTypeServiceComponent } from '@fuse/components/autocomplete/servicetype/autocomplete.service.type.component';

@Component({
    selector: 'plan-form',
    templateUrl: './plan.form.component.html',
    styleUrls: ['./plan.form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class PlanFormComponent implements OnInit {

    @ViewChild(AutocompleteTypeServiceComponent) autocomplete: AutocompleteTypeServiceComponent;
    id: number;
    modelForm: FormGroup;
    modelFormErrors: any;
    processing: boolean;

    servicesType: Array<TypeServicePlanModel>;

    days: number;

    price: number;
    minutes: number;

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder, private service: PlanBusiness
    ) {

        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

        this.activeRoute.params.subscribe(params => {
            if (params['parameters']) {
                this.id = params['parameters'];
            }
        });

        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });

    }

    save(event) {
        let entity: PlanModel = new PlanModel();
        entity.id = this.id;
        entity.price = this.price;
        entity.minutes = ((this.days * 24) * 60);
        entity.days = this.days;
        entity.cost = 0;
        entity.services = this.autocomplete.selected;
        this.processing = true;
        
        this.service.merge(entity).subscribe(
            success => {
                this.router.navigate(['/ADM/ADM05']);
            }, error => {
                this.processing = false;
            }

        );

    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    ngOnInit(): void {
        this.modelFormErrors = {
            price: {},
            days: {},
        };
        if (this.id) {
            this.service.findById(this.id).subscribe(
                success => {
                    this.id = success.id;
                    this.price = success.price;
                    this.minutes = success.minutes;
                    this.days = ((this.minutes / 60) / 24);
                    this.servicesType = success.services;
                }
            );
        }
    }


    createForm() {
        return this.formBuilder.group({
            id: [this.id],
            price: [this.price, [Validators.required, Validators.pattern("^[0-9]{1,3}([,.][0-9]{1,2})?$")]],
            days: [this.days, [Validators.required, Validators.pattern("^[0-9]*$")]],
        });
    }

}
