import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';

import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSidenavModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
import { LoggedGuard } from '@fuse/guard/looged.guard';

import { TemplateModule } from 'app/main/content/template/template.module';
import { ServiceModule } from 'app/service/service.module';
import { TypeServicePlanComponent } from './type.service.plan.component';
import { TypeServicePlanFormComponent } from './form/type.service.plan.form';



const routes: Routes = [
    {
        path: 'ADM11',
        component: TypeServicePlanComponent,
    },
    {
        path: 'ADM11/form/:parameters',
        component: TypeServicePlanFormComponent,
    },
    {
        path: 'ADM11/form',
        component: TypeServicePlanFormComponent,
    }


];

@NgModule({
    declarations: [
        TypeServicePlanComponent,
        TypeServicePlanFormComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CdkTableModule,
        TemplateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSidenavModule,
        MatTableModule,
        MatToolbarModule,
        FuseSharedModule,
        FuseConfirmDialogModule,
        ServiceModule,
    ]
})
export class TypeServicePlanModule {
}
