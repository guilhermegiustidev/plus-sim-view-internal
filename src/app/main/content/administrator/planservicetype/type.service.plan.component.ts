import { Component, ViewEncapsulation } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { ProviderPlanBusiness } from 'app/service/provider/provider.plan.service';
import { ProviderPlanModel } from 'app/model/provider/providerplan.model';
import { TypeServicePlanBusiness } from 'app/service/plan/type.service.plan.service';


@Component({
    templateUrl: './type.service.plan.component.html',
    styleUrls: ['./type.service.plan.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class TypeServicePlanComponent {

    filter: ProviderPlanModel;

    columns = [
        {prop: 'id', name: 'TYPE_PLAN_SERVICE.TABLE.HEADER.ID'},
        {prop: 'name', name: 'TYPE_PLAN_SERVICE.TABLE.HEADER.NAME'},        
    ]

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private business: TypeServicePlanBusiness
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
    }

}
