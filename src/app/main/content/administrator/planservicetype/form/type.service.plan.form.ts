import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { ProviderPlanBusiness } from 'app/service/provider/provider.plan.service';
import { TypeServicePlanBusiness } from 'app/service/plan/type.service.plan.service';

@Component({
    templateUrl: './type.service.plan.form.html',
    styleUrls: ['./type.service.plan.form.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class TypeServicePlanFormComponent implements OnInit {

    id: number;
    modelForm: FormGroup;
    modelFormErrors: any;
    processing: boolean;
    name: string;
    status: boolean;


    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder, private service: TypeServicePlanBusiness
    ) {

        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

        this.activeRoute.params.subscribe(params => {
            if (params['parameters']) {
                this.id = params['parameters'];
            }
        });

        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });

    }

    save(event) {
        this.processing = true;
        this.service.merge({id: this.id, name: this.name, status: this.status}).subscribe(
            success=>{
                this.processing = false;
                this.router.navigate(['/ADM/ADM11'])
            }, error => {
                this.processing = false;
            }
        );
    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    ngOnInit(): void {
        this.modelFormErrors = {
            name: {}
        };
        this.status = true;
        if (this.id) {
            this.service.findById(this.id).subscribe(
                success=>{
                    this.name = success.name;
                    this.status = success.status;
                }
            );
        }
    }


    createForm() {
        return this.formBuilder.group({
            name: [this.name, Validators.required]
        });
    }

}
