export const locale = {
    lang: 'en',
    data: {
        'TYPE_PLAN_SERVICE': {
            'HEADER': {
                'TITLE': 'Cadastro de Tipos de Serviços'
            },
            'TABLE': {
                'HEADER': {
                    'ID': 'Id',
                    'NAME': 'Nome'
                }
            },
            'FORM': {
                'NAME': 'Nome',
                'VALIDATOR': {
                    'NAME': {
                        'REQUIRED': 'Nome é obrigatório'
                    }

                }
            },
        }
    }
};
