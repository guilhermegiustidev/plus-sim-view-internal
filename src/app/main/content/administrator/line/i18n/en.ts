export const locale = {
    lang: 'en',
    data: {
        'LINE': {
            'HEADER': {
                'TITLE': 'Cadastro de Linhas'
            },
            'TABLE': {
                'HEADER': {
                    'ID': 'Id',
                    'NUMBER': 'Número',
                    'RELEASED': 'Data de liberação',
                    'STATUS': 'Status'
                }
            },
            'FORM': {
                'REGION': 'Região',
                'PROVIDER': 'Dados de operadora',
                'NUMBER': 'Número',
                'COUNTRY': 'País',
                'STATUS': 'Status',
                'RELEASED' : 'Data de liberação',
                'FORNECEDOR': 'Fornecedor',
                'PROVIDER_PLAN' : 'Planos do Fornecedor',
                'VALIDATOR': {
                    'PROVIDER_PLAN' : {
                        'REQUIRED': 'Plano é obrigatório'
                    },

                    'COUNTRY': {
                        'REQUIRED': 'País obrigatório',
                    },
                    'NUMBER': {
                        'REQUIRED': 'Número obrigatório',
                        'INVALID': 'Número inválido'
                    },
                }
            },
        }
    }
};
