import { Component, ViewEncapsulation } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from './i18n/en';
import { locale as portuguese } from './i18n/pt';
import { locale as spanish } from './i18n/sp';
import { LineModel } from '../../../../model/line.model';
import { LineBusiness } from '../../../../service/line/line.service';


@Component({
    templateUrl: './line.component.html',
    styleUrls: ['./line.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class LineComponent {

    filter: LineModel;

    columns = [
        {prop: 'id', name: 'LINE.TABLE.HEADER.ID'},
        {prop: 'number', name: 'LINE.TABLE.HEADER.NUMBER'},
        {prop: 'released', name: 'LINE.TABLE.HEADER.RELEASED'},        
        {prop: 'status', name: 'LINE.TABLE.HEADER.STATUS'},
    ]

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private lineBusiness: LineBusiness
    ) {
        this.fuseTranslationLoader.loadTranslations(english, portuguese, spanish);
    }

}
