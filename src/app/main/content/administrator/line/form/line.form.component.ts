import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { fuseAnimations } from '@fuse/animations';

import { locale as english } from '../i18n/en';
import { locale as portuguese } from '../i18n/pt';
import { locale as spanish } from '../i18n/sp';
import { LineBusiness } from '../../../../../service/line/line.service';
import { CountryBusiness } from 'app/service/location/country.service';
import { CountryModel } from 'app/model/country.model';
import { LineModel } from 'app/model/line.model';
import { DatePipe } from '@angular/common';
import { ProviderModel } from 'app/model/provider/provider.model';
import { ProviderBusiness } from 'app/service/provider/provider.service';
import { ProviderPlanBusiness } from 'app/service/provider/provider.plan.service';
import { ProviderPlanModel } from 'app/model/provider/providerplan.model';
import { CityComponent } from '@fuse/components/city/city.component';
import { CityBusiness } from 'app/service/location/city.service';

@Component({
    selector: 'line-form',
    templateUrl: './line.form.component.html',
    styleUrls: ['./line.form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class LineFormComponent implements OnInit {
    @ViewChild(CityComponent) cityComponent: CityComponent;

    id: number;
    modelForm: FormGroup;
    modelFormErrors: any;
    processing: boolean;

    number: string;
    selectedFornecedor: number;
    selectedPlanFornecedor: number;
    fornecedores: Array<ProviderModel>;
    fornecedoresPlans: Array<ProviderPlanModel>;
    fornecedor : ProviderModel;
    fornecedorPlan: ProviderPlanModel;
    status: string;
    released: Date;

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private fuseTranslationLoaderService: FuseTranslationLoaderService,
        private formBuilder: FormBuilder, private service: LineBusiness,
        private providerBusiness: ProviderBusiness, private planProviderPlan: ProviderPlanBusiness,
        private cityBusiness: CityBusiness, private countryBusiness: CountryBusiness
    ) {
        this.providerBusiness.findAll().subscribe(
            success => {
                this.fornecedores = success;
            }
        );
        this.fuseTranslationLoaderService.loadTranslations(english, portuguese, spanish);

        this.activeRoute.params.subscribe(params => {
            if (params['parameters']) {
                this.id = params['parameters'];
            }
        });

        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });

    }

    save(event) {
            let parameter = {
                id          	: this.id,
                number      	: this.number,
                status      	: this.status,
                released    	: this.released,
                provider		: this.fornecedor,
                city            : this.cityComponent.city,
                country         : this.cityComponent.city.state.country,
                providerPlan 	: this.fornecedorPlan
            }
            
            this.service.merge(parameter).subscribe(
                success => {
                    this.router.navigate(['/ADM/ADM03']);
                }, 
                error => {
                    this.processing = false;
                }
            );
            
    }

    changeFornecedor(event){
        if(this.selectedFornecedor){
            this.providerBusiness.findById(this.selectedFornecedor).subscribe(
                success => {
                    this.fornecedor = success;
                    this.planProviderPlan.findPlanProviderByProviderId(this.fornecedor.id).subscribe(
                        success => {
                            this.fornecedoresPlans = success;
                        }
                    )
                }
            );
        }
    }

    changePlan(event){
        if(this.selectedPlanFornecedor){
            this.planProviderPlan.findById(this.selectedPlanFornecedor).subscribe(
                success => {
                    this.fornecedorPlan = success;
                }
            );
        }
    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    ngOnInit(): void {
        this.modelFormErrors = {
            number: {},
            fornecedor: {},
            planFornecedor: {},
            name: {}
        };
        if (this.id) {
            this.service.findById(this.id).subscribe(
                success => {
                    this.number = success.number;
                    this.status = success.status;
                    this.released = success.released;
                }
            );
        }
    }


    createForm() {
        var datePipe = new DatePipe('dd/MM/yyyy');

        return this.formBuilder.group({
            id: [this.id],
            status: [{ value: this.status, disabled: this.id }],
            released: [{ value: datePipe.transform(this.released, 'dd/MM/yyyy'), disabled: this.id }],
            fornecedor: [this.selectedFornecedor, Validators.required],
            planFornecedor: [this.selectedPlanFornecedor, Validators.required],
            number: [this.number, [Validators.required, Validators.pattern("^[0-9]*$")]],
        });
    }

}
