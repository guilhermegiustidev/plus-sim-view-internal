import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';

import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatRippleModule, MatSidenavModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
import { LoggedGuard } from '@fuse/guard/looged.guard';

import { TemplateModule } from 'app/main/content/template/template.module';
import { ServiceModule } from 'app/service/service.module';

import { AddressModule } from '@fuse/components/address/address.module';

import { LineComponent } from './line.component';
import { LineFormComponent } from './form/line.form.component';
import { CityComponent } from '@fuse/components/city/city.component';

const routes: Routes = [
    {
        path: 'ADM03',
        component: LineComponent,
    },
    {
        path: 'ADM03/form/:parameters',
        component: LineFormComponent,
    },
    {
        path: 'ADM03/form',
        component: LineFormComponent,
    }


];

@NgModule({
    declarations: [
        LineComponent,
        LineFormComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        CdkTableModule,
        TemplateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSidenavModule,
        MatTableModule,
        MatToolbarModule,
        FuseSharedModule,
        FuseConfirmDialogModule,
        ServiceModule,
    ]
})
export class LineModule {
}
